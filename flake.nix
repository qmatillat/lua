{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = {
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      build =
        pkgs.runCommand "build-book" {}
        "${pkgs.mdbook}/bin/mdbook build -d $out ${./.}";
    in {
      packages = {
        default = build;
      };
      formatter = pkgs.alejandra;
      devShells.default = pkgs.mkShell {
        packages = with pkgs; [mdbook];
      };
    });
}
