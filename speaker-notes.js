(function() {
    let allNotes = document.querySelectorAll("details");

    for(const notes of allNotes) {
        let summary = document.createElement("summary");
        notes.insertBefore(summary, notes.firstChild);

        let h4 = document.createElement("h4");
        h4.append("Notes");
        summary.append(h4);
    }
})();
