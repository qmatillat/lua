# Syntaxe basique

Quelques points sur la syntaxe :
* Les blocs sont définis par des mots clef (`do` `then` `else` `end` ...).
* Les commentaires de ligne commencent avec `--`.
* Les commentaires bloc sont délimités par `--[[ --]]`.
* La déclaration des variables se fait avec `local`
  - L'assignation avec `=`.
  - Les comparaisons avec `==`, `~=`, `<`, `<=`, `>`, `>=`.

```lua,editable
--[[
    Experience avec des variables et des comparaisons
--]]
local dix = 10
print(dix == 10, dix ~= 15)
local quinze = 15 -- 2° variable
print(dix < quinze, quinze >= 15, 12 > 7)

local function printAge(age)
    print("J'ai " .. age .. " ans")
end

printAge(15)
printAge(30)
```
