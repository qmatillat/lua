# Pourquoi le Lua ?

Les principaux avantages du Lua sont :

* La portabilité : Facile à exécuter sur différents types de machines.
* L'extensibilité : Lua est très basique et offre pleins d'options pour ajouter
  des extensions.
* La simplicité.
* L'efficacité.
* L'intégrabilité : Lua a été conçu, depuis le début, pour être intégré dans
  des logiciels. Il est facile d'ajouter la possibilité d'exécuter du Lua dans
  un autre programme (comme un moteur de jeu). C'est le cas notamment de
  **Babel** (l'éditeur de The Crew), de **Reaper** (logiciel MAO) et de plein
  d'autres logiciels.

