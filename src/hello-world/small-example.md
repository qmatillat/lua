# Exemple plus complexe

Voici un programme un peu plus compliqué en Lua :

```lua,editable
local x = 6;        -- Variable avec une valeur
print("Début", x)
while x ~= 1        -- Pas de parenthèse autour de la condition, ~= pour différent
do                  -- do / end pour les bornes de la boucle
    if x % 2 == 0
    then            -- then / else / end pour les bornes de la condition
        x = x / 2
    else
        x = 3 * x + 1
    end
    print("Valeur", x)
end
```

<details>

Ce code implémente la [conjecture de Syracuse][syracuse].

On pense que cette boucle s'arrêtera forcément, mais cela n'a jamais été
prouvé.

Changez le code et essayer avec différentes valeurs
</details>

[syracuse]: https://fr.wikipedia.org/wiki/Conjecture_de_Syracuse
