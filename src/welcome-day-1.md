# Session 1

Le but de cette session est de découvrir les base de LUA :

* Types et valeurs : nombres, chaîne de cratères, booléen, `nil`, tables,
  méta-tables
* Expressions et déclarations
* Instructions
* Fonctions
