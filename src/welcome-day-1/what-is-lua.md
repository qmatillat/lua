# Qu'est-ce que le LUA ?


D'après [lua.org](http://www.lua.org/manual/5.4/manual.html):
> Lua est un langage de **script** puissant, efficace, léger et intégrable. [...]
>
> Lua est typé **dynamiquement**, [...] ce qui le rend idéal pour la
> configuration, les scripts et le prototypage rapide.
