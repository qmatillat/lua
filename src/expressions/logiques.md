# Logiques

Les opérateurs logiques sont `and`, `or`, et `not`.

L'opérateur `and` renvoie son premier argument s'il est équivalent à faux ; Sinon, il renvoie son deuxième argument.

L'opérateur `or` renvoie son premier argument s'il n'est pas équivalent à faux ; Sinon, il renvoie son deuxième argument.

`and` et `or` utilisent une évaluation raccourcie, c'est-à-dire qu'ils évaluent
leur deuxième opérande uniquement si nécessaire.
```lua,editable
local function return_true()
    print("Fonction")
    return true
end

print("a")
local a = true or return_true()
print("b")
local b = false and return_true()

print("c")
local c = true and return_true()
print("d")
local d = false or return_true()
```


## Idiomes

### Valeur par défaut
Un idiome Lua utile est `x = x or v`, qui équivaut à
```lua,editable
if not x
then
    x = v
end
```
Cela permet de définir une valeur par défaut pour `x`.

### Opérateur ternaire
Un autre idiome utile (quoiqu'un peu obscure) est `a and b or c`
```lua,editable
if a
then
    return b
else
    return c
end
```
Par exemple, pour calculer le maximum, on peut utiliser `(x > y) and x or y`.

**Attention à la lisibilité**

