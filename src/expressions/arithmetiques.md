# Arithmétiques

Lua supporte les opérateurs arithmétiques classiques : 
- Additions `+`
- Soustractions `-`
- Multiplications `*`
- Division `/` et Modulo `%`
- L'exponentiation `^` (support partiel)

Lua supporte aussi l'opérateur unaire de négation `-`.

```lua,editable
print(-2)
print(1 + 2 * 3 / 5^3)
```
