# Expressions

Les expressions représentent les valeurs.

Les expressions dans LUA incluent :
- les constantes numériques (`5`, `1e2`)
- les littéraux de chaîne (`"chaîne"`)
- les variables
- les opérations unaires et binaires
- les appels de fonctions.
