# Relationnelles

Lua fournit les opérateurs relationnels suivants : `<` `>` `<=` `>=` `==` `~=`

Tous ces opérateurs retournent toujours `true` ou `false`.

### (In)Égalité
L'opérateur `==` teste l'égalité ; L'opérateur `~=` est la négation de l'égalité.

Si les valeurs ont des types différents, LUA les considère comme différentes
valeurs. Sinon, Lua les compare en fonction de leurs types. Plus précisément,
NIL est égal à lui-même.

LUA compare les tables et les fonctions par référence, c'est-à-dire que deux de
ces valeurs sont considérées comme égales uniquement si elles sont le même
objet.

```lua,editable
a = {}; a.x = 1; a.y = 0
b = {}; b.x = 1; b.y = 0 
c = a
print(a == c, a ~= b)
```

### Comparaison
Nous pouvons appliquer les opérateurs de comparaison uniquement à deux nombres ou
à deux chaînes (ordre alphabétique).

### Types
Lorsque vous comparez des valeurs avec différents types, vous devez être
prudent : n'oubliez pas que :
- `"0" ~= 0`
- `2 < 15`, mais `"2" > "15"`

Pour éviter des resultats incohérents, LUA lève une erreur lorsque vous
mélangez des chaînes et des nombres dans une comparaison de commande, comme `2
< "15"`.

```lua,editable
print(5 == "5")
print(5 == -(-"5"))
```
