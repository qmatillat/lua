# Hello World !

Commençons avec un des programmes les plus simples, l'affichage du classique
`Hello World` :

```lua,editable
function hello()
    print("Hello 🌍!");
end

hello()
```

Ce qu'on peut voir :

* Les fonctions sont définies avec le mot clé `function`.
* Les blocs sont délimités par des mots clef (ici `end`)
* Lua exécute tout ce qui est dans le fichier, il n'y a pas de points d'entrée
  (`main`).

