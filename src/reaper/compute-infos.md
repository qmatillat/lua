# Calcul des infos

[Documentation](https://www.reaper.fm/sdk/reascript/reascripthelp.html#l)

On a besoins de :
- la durée de l'automation
- le début de chaque automation

<details>

```lua
---@param track MediaTrack
local function getAutomationLength(track)
    local numMediaItem = reaper.CountTrackMediaItems(track)

    local minDuration = math.huge
    for i = 0, numMediaItem - 1 do
        local mediaItem = reaper.GetTrackMediaItem(track, i)

        local pos = reaper.GetMediaItemInfo_Value(mediaItem, "D_POSITION")
        print(pos)

        local duration = reaper.GetMediaItemInfo_Value(mediaItem, "D_LENGTH")
        minDuration = math.min(duration, minDuration)
    end
    return minDuration
end
```

</details>
