# Retour d'erreur

[Documentation](https://www.reaper.fm/sdk/reascript/reascripthelp.html#l)

<details>

```lua
print("Message dans la console (native)")
reaper.ShowConsoleMsg("Message dans la console (reaper)")

-- Type   0=OK 1=OKCANCEL 2=ABORTRETRYIGNORE 3=YESNOCANCEL 4=YESNO 5=RETRYCANCEL 
-- Retour 1=OK 2=CANCEL 3=ABORT 4=RETRY 5=IGNORE 6=YES 7=NO
reaper.ShowMessageBox("Message d'erreur", "Error", 0)
```

</details>

On peut simplifier le code avec une petite fonction:
<details>

```lua
---@param msg string
local function error(msg)
    reaper.ShowMessageBox(msg, "Error", 0)
end
```

</details>
