# Script final

```lua
---@param msg string
local function error(msg)
    reaper.ShowMessageBox(msg, "Error", 0)
end

---@param name string
---@param f function
local function doUndo(name, f)
    reaper.Undo_BeginBlock()
    f()
    reaper.Undo_EndBlock(name, 0)
end

---@param track MediaTrack
local function getAutomationLength(track)
    local numMediaItem = reaper.CountTrackMediaItems(track)

    local minDuration = math.huge
    for i = 0, numMediaItem - 1 do
        local mediaItem = reaper.GetTrackMediaItem(track, i)
        local duration = reaper.GetMediaItemInfo_Value(mediaItem, "D_LENGTH")

        minDuration = math.min(duration, minDuration)
    end
    return minDuration
end

local track = reaper.GetSelectedTrack(0, 0)
if not track then
    error("Please select a track as master")
    return
end

local envelope = reaper.GetSelectedEnvelope(0)
if not envelope then
    error("Please select an envelope to control")
    return
end

local automationLength = getAutomationLength(track)
local mediaItemCount = reaper.CountTrackMediaItems(track)

doUndo("SideChain", function()
    local poolId = -1

    for i = 0, mediaItemCount - 1 do
        local mediaItem = reaper.GetTrackMediaItem(track, i)
        local pos = reaper.GetMediaItemInfo_Value(mediaItem, "D_POSITION")

        local automationItemId = reaper.InsertAutomationItem(envelope, poolId, pos, automationLength)
        if poolId == -1 then
            poolId = reaper.GetSetAutomationItemInfo(envelope, automationItemId, "D_POOL_ID", 0, false)
        end
    end
end)
```
