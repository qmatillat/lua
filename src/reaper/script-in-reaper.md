# Scripter dans Reader

Dans reaper, chaque script vit dans une action :
![Actions](./actions.png)

Pour créer un script, on crée une action type `ReaScript`.
![Création script](./createScript.png)


Exemple de Lua pour afficher une fenêtre :
```lua
if reaper.ShowMessageBox("Message", "Title", 1) == 1
then
    reaper.ShowConsoleMsg("OK")
end
```
![Fenêtre de script](./scriptWindow.png)
