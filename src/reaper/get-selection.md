# Accéder a la sélection

[Documentation](https://www.reaper.fm/sdk/reascript/reascripthelp.html#l)

## Track

<details>

```lua
local track = reaper.GetSelectedTrack(0, 0)
```

</details>

## Envelope

<details>

```lua
local envelope = reaper.GetSelectedEnvelope(0)
```

</details>
