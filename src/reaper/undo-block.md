# Bloc d'annulation

Pour simplifier l'intégration de la commande dans Reaper, on peut utiliser un
bloc d'annulation pour ajouter l'action comme atomique dans l'historique.

<details>

```lua
reaper.Undo_BeginBlock()
-- Code
reaper.Undo_EndBlock("myScript", 0)
```
</details>

<br />

<details>

```lua
---@param name string
---@param f function
local function doUndo(name, f)
    reaper.Undo_BeginBlock()
    f()
    reaper.Undo_EndBlock(name, 0)
end

doUndo("myScript", function()
    -- Code
end)
```
</details>
