--- Configuration
local window = 0.1 -- sec
local blurSize = 5

local minSound = 0
local minDim = 716.21785031261

local maxSound = 0.03
local maxDim = 600
--- Configuration

---@class Entry
---@field time number
---@field value number

---@param x number
---@param min number
---@param max number
---@return number
local function clamp(x, min, max)
    return math.max(math.min(x, max), min)
end

---@param x number
---@param in_min number
---@param in_max number
---@param out_min number
---@param out_max number
---@return number
local function mapRange(x, in_min, in_max, out_min, out_max)
    local in_range = in_max - in_min
    local out_range = out_max - out_min

    local min = math.min(out_min, out_max)
    local max = math.max(out_min, out_max)

    return clamp(out_min + (x - in_min) * out_range / in_range, min, max)
end

---@param msg string
local function error(msg)
    reaper.ShowMessageBox(msg, "Error", 0)
end

---@param name string
---@param f function
local function doUndo(name, f)
    reaper.Undo_BeginBlock()
    f()
    reaper.Undo_EndBlock(name, 0)
end

---@param envelope TrackEnvelope
---@param peaks Entry[]
---@param time_start number
---@param time_end number
function BuildPointsFromTable(envelope, peaks, time_start, time_end)
    reaper.DeleteEnvelopePointRangeEx(envelope, -1, time_start, time_end)

    -- create automation
    local automationItemId = reaper.InsertAutomationItem(envelope, -1, time_start, time_end - time_start)
    reaper.GetSetAutomationItemInfo_String(envelope, automationItemId, 'P_POOL_NAME', "autoVol", true)

    for _, val in pairs(peaks) do
        reaper.InsertEnvelopePointEx(
            envelope,
            automationItemId,
            --[[     time ]] val.time,
            --[[    value ]] mapRange(val.value, minSound, maxSound, minDim, maxDim),
            --[[    shape ]] 0,
            --[[  tension ]] 0,
            --[[ selected ]] false,
            --[[   noSort ]] true
        )
    end

    reaper.Envelope_SortPointsEx(envelope, automationItemId)
    reaper.UpdateArrange()
end

---@param envelope TrackEnvelope
---@param ts_start number
---@param ts_end number
---@return Entry[]
function GetPeaks(track, ts_start, ts_end)
    ---@type Entry[]
    local out = {}

    local accessor = reaper.CreateTrackAudioAccessor(track)

    local freq = tonumber(reaper.format_timestr_len(1, '', 0, 4))
    local buffer_size = math.floor(freq * window)

    local buf = reaper.new_array(buffer_size)
    for pos = ts_start, ts_end, window do
        reaper.GetAudioAccessorSamples(accessor, freq, 1, pos, buffer_size, buf)
        local sum = 0
        local cnt = 0
        for spl = 1, buffer_size do
            if buf[spl] then
                cnt = cnt + 1
                sum = sum + math.abs(buf[spl])
            end
        end

        table.insert(out, {
            time = pos + window / 2,
            value = sum / cnt,
        })
    end
    reaper.DestroyAudioAccessor(accessor)
    return out
end

---@param t Entry[]
---@return Entry[]
local function Blur(t)
    local out = {}
    for i = 1, #t do
        local v = t[i].value
        local n = 1
        for delta = 1, blurSize do
            if t[i + delta] then
                v = v + t[i + delta].value
                n = n + 1
            end

            if t[i - delta] then
                v = v + t[i - delta].value
                n = n + 1
            end
        end

        table.insert(out, {
            time = t[i].time,
            value = v,
        })
    end
    return out
end

-- Main

local ts_start, ts_end = reaper.GetSet_LoopTimeRange(false, false, 0, 0, false)
if ts_end - ts_start < 0.001 then
    error("Empty loop range")
    return
end

local item = reaper.GetSelectedMediaItem(0, 0)
if not item then
    error("No selected item")
    return
end

local track = reaper.GetMediaItem_Track(item)
if not track then
    error("No track in selected item")
    return
end

local envelope = reaper.GetSelectedEnvelope(0)
if not envelope then
    error("Please select an envelope to control")
    return
end


local peaks = GetPeaks(track, ts_start, ts_end)
peaks = Blur(peaks)
doUndo("SideChain", function() BuildPointsFromTable(envelope, peaks, ts_start, ts_end) end)
