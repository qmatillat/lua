# Reaper

*ReaScript* est le nom généralisé qui fait référence aux programmes capables de
contrôler REAPER. Il existe différents langages de programmation qui peuvent
être utilisés : 
- Lua
- Python
- EEL (langage interne open-source)
- C
