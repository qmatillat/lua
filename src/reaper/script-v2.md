# Amélioration

```lua
--- Configuration
local window = 0.002
local threshold = -20
local hold_release = 0.05
--- Configuration

---@param msg string
local function error(msg)
    reaper.ShowMessageBox(msg, "Error", 0)
end

---@class Entry
---@field pos number
---@field val number

---@param name string
---@param f function
local function doUndo(name, f)
    reaper.Undo_BeginBlock()
    f()
    reaper.Undo_EndBlock(name, 0)
end

function ConvertDbToValue(x) return math.exp((x) * math.log(10) / 20) end

local threshold_linear = ConvertDbToValue(threshold)

---@param envelope TrackEnvelope
---@param peaks Entry[]
---@param time_start number
---@param time_end number
function BuildPointsFromTable(envelope, peaks, time_start, time_end)
    -- get base len for automation
    local min_len = math.huge
    for i = 1, #peaks - 1 do
        min_len = math.min(min_len, peaks[i + 1].pos - peaks[i].pos)
    end

    -- create automation
    local poolId = -1
    for i = #peaks, 1, -1 do
        local automationItemId = reaper.InsertAutomationItem(envelope, poolId, peaks[i].pos, min_len)
        reaper.GetSetAutomationItemInfo(envelope, automationItemId, 'D_LOOPSRC', 0, true)

        if poolId == -1 then
            poolId = reaper.GetSetAutomationItemInfo(envelope, automationItemId, "D_POOL_ID", 0, false)
        end
    end

    reaper.Envelope_SortPointsEx(envelope, -1)
    reaper.UpdateArrange()
end

---@param track MediaTrack
---@param ts_start number
---@param ts_end number
---@return Entry[]
function GetPeaks(track, ts_start, ts_end)
    local out = {}
    local accessor = reaper.CreateTrackAudioAccessor(track)

    local freq = tonumber(reaper.format_timestr_len(1, '', 0, 4))
    local buffer_size = math.floor(freq * window)

    local buf = reaper.new_array(buffer_size)
    for pos = ts_start, ts_end, window do
        reaper.GetAudioAccessorSamples(accessor, freq, 1, pos, buffer_size, buf)
        local sum = 0
        local cnt = 0
        for spl = 1, buffer_size do
            if buf[spl] then
                cnt = cnt + 1
                sum = sum + math.abs(buf[spl])
            end
        end
        table.insert(out, {
            pos = pos,
            val = sum / cnt,
        })
        buf:clear()
    end
    reaper.DestroyAudioAccessor(accessor)
    return out
end

---@param t Entry[]
function FilterPeaks(t)
    -- threshold
    local idremove = {}
    for i = 1, #t do
        if t[i].val < threshold_linear then table.insert(idremove, i) end
    end
    for i = #idremove, 1, -1 do table.remove(t, idremove[i]) end

    -- hold_release
    local last_cur_pos
    idremove = {}
    for i = 1, #t do
        local cur_pos = t[i].pos
        if last_cur_pos and cur_pos - last_cur_pos < hold_release then table.insert(idremove, i) end
        last_cur_pos = cur_pos
    end
    for i = #idremove, 1, -1 do table.remove(t, idremove[i]) end
end

local ts_start, ts_end = reaper.GetSet_LoopTimeRange(false, false, 0, 0, false)
if ts_end - ts_start < 0.001 then
    error("Empty loop range")
    return
end

local item = reaper.GetSelectedMediaItem(0, 0)
if not item then
    error("No selected item")
    return
end

local track = reaper.GetMediaItem_Track(item)
if not track then
    error("No track in selected item")
    return
end

local envelope = reaper.GetSelectedEnvelope(0)
if not envelope then
    error("Please select an envelope to control")
    return
end


local peaks = GetPeaks(track, ts_start, ts_end)
FilterPeaks(peaks)
doUndo("SideChain", function() BuildPointsFromTable(envelope, peaks, ts_start, ts_end) end)
```
