# Créations des automations

[Documentation](https://www.reaper.fm/sdk/reascript/reascripthelp.html#l)

<details>

```lua
local automationItemId = reaper.InsertAutomationItem(envelope, poolId or -1, pos, automationLength)
local poolId = reaper.GetSetAutomationItemInfo(envelope, automationItemId, "D_POOL_ID", 0, false)
```
</details>
