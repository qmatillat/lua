# Étapes du script

<details>

- Récupération des items média / automation / enveloppes.
  - Retour d'erreur en cas de problème.
- Détections des "pics".
- Calcul du nombre et de la durée de chaque automation.
- Création des automations.

</details>
