# Détection des "pics"

Dans la première version, nous allons utiliser `Dynamic split items`
<kbd>D</kbd>, mais on pourrait détecter les pics depuis LUA.

<details>

```reaper.CreateTrackAudioAccessor```

</details>

<br/>

<details>
<summary>Solution</summary>

```lua
function GetPeaks(track, start, end)
    local out = {}
    local accessor = reaper.CreateTrackAudioAccessor(track)

    local freq = tonumber(reaper.format_timestr_len(1, '', 0, 4))
    local buffer_size = math.floor(freq * window)

    local buf = reaper.new_array(buffer_size)
    for pos = start, end, window do
        reaper.GetAudioAccessorSamples(accessor, freq, 1, pos, buffer_size, buf)
        local sum = 0
        local cnt = 0
        for spl = 1, buffer_size do
            if buf[spl] then
                cnt = cnt + 1
                sum = sum + math.abs(buf[spl])
            end
        end
        out[#out + 1] = { pos = pos, val = sum / cnt }
        buf.clear()
    end
    reaper.DestroyAudioAccessor(accessor)
    return out
end

function FilterPeaks(t)
    -- Filter sample that are above threshold
    local idremove = {}
    for i = 1, #t do if t[i].val < threshold_linear then idremove[#idremove + 1] = i end end
    for i = #idremove, 1, -1 do table.remove(t, idremove[i]) end

    -- Filter for hold_release
    local cur_pos, last_cur_pos
    local idremove = {}
    for i = 1, #t do
        cur_pos = t[i].pos
        if last_cur_pos and cur_pos - last_cur_pos < hold_release then idremove[#idremove + 1] = i end
        last_cur_pos = cur_pos
    end
    for i = #idremove, 1, -1 do table.remove(t, idremove[i]) end
end
```

</details>
