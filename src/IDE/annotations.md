# Annotations

[Documentation](https://luals.github.io/wiki/annotations/)

```lua
---@param a number
---@param b number
---@return number
function Max(a, b)
    if a > b then
        return a
    else
        return b
    end
end

---@alias Cell "·" | "*" | number
---@alias Grid Cell[][]

---@type Grid
local base = {
    { "·", "*", "·", "*", "·" },
    { "·", "·", "*", "·", "·" },
    { "·", "·", "*", "·", "·" },
    { "·", "·", "·", "·", "·" },
}

---@enum Colors
local COLORS = {
	black = 0,
	red = 2,
	green = 4,
	yellow = 8,
	blue = 16,
	white = 32
}

---@param color colors
local function setColor(color) end

setColor(COLORS.green)
```
