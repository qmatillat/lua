# Exemple de code

Dans ce cours, nous allons explorer le LUA aux travers d'exemple qui peuvent
être exécuté directement dans le navigateur.

Les exemples de code sont interactifs :

```lua,editable
print("Changez moi !")
```

*Vous pouvez utiliser <kbd>Ctrl</kbd> <kbd>Enter</kbd> pour lancer le code
lorsque vous êtes dans l'éditeur*
