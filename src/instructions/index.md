# Instructions

Chaque ligne d'un programme Lua représente une instruction.

Il existe plusieurs types d'instructions :
- Les affectations
- Les appel de fonctions
- Les instructions de contrôle de flux
