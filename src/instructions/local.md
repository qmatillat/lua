# Variables locales

Lua support les variables locales et globales. Contrairement aux variables
globales, les variables locales ont leur portée limitée au bloc où elles sont
déclarées. Un bloc est le corps d'une structure de contrôle, le corps d'une
fonction.

```lua,editable
x = 10
local i = 1        -- locale au fichier

while i<=x do
  local x = i*2    -- locale au corps du while
  print(x)         --> 2, 4, 6, 8, ...
  i = i + 1
end

if i > 20 then
  local x          -- locale au "then"
  x = 20
  print(x + 2)
else
  print(x)         --> 10 (global)
end

print(x)           --> 10 (global)
```

C'est un bon style de programmation pour utiliser les variables locales chaque
fois que possible. Les variables locales vous aident à éviter de polluer
l'environnement global avec des noms inutiles.
