# `break` et `return`

Les instructions `break` et `return` nous permettent d'arrêter l'exécution d'un
bloc.

Vous utilisez l'instruction `break` pour terminer une boucle. Cette déclaration
rompt la boucle intérieure qui le contient. Après le `break`, le programme
continue au point immédiatement après la boucle cassée.

Une instruction `return` termine l'exécution de la fonction, et spécifie les
valeurs de retours. Il y a un retour implicite à la fin de toute fonction, vous
n'avez donc pas besoin d'en utiliser un si votre fonction se termine
naturellement sans renvoyer de valeur.

```lua,editable
local function search(needle, haystack)
    for idx,val in pairs(haystack) do
        if val == needle then
            return idx
        end
    end
end

local array = {"a", "b", "c", "d", "e"}

print(search("d", array), "d")
print(search("j", array), "j")
```
