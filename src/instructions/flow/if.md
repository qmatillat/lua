# `if then else`

Une instruction `if` teste son la condition et exécute sa partie `then` ou `else` en conséquence. Le bloc `else` est optionel.
```lua,editable
local a = -5
local b = 2

if a<0 then a = 0 end

if a<b then print("a =", a) else print("b =", b) end

if a > 10 then
    if ask_reset_a() then
        a = 0
    end
end
```

Lorsque vous écrivez `if` imbriqué, vous pouvez utiliser `elseif` (en un mot).
Il est similaire à un `else` suivi d'un `if`, mais il évite le `end`:

```lua,editable
local a,b,r = 5,2
local op = "*"

if op == "+" then
    r = a + b
elseif op == "-" then
    r = a - b
elseif op == "*" then
    r = a*b
elseif op == "/" then
    r = a/b
else
    error("invalid operation")
end

print(r)
```
