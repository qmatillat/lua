# `while`

Lua teste d'abord la condition du `while` : si la condition est fausse, la
boucle se termine ; Sinon, Lua exécute le corps de la boucle et répète le
processus.

```lua,editable
local i = 10
while i > 0 do
  print(i)
  i = i - 1
end
```
