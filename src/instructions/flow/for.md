# `for`

L'instruction `for` a deux variantes : la version numérique et la version
*générique*.

## `for` numérique
Un `for` numérique pour a la syntaxe suivante :

```lua,editable
for i = 1,10,2 do
    print(i)
    -- On peut changer i dans la boucle,
    -- mais ça n'as pas d'impact sur les autres intérations
    i = 0
    print("i = 0: ", i)
end
```

La variable créée est une variable locale déclarée automatiquement et n'est
visible uniquement à l'intérieur de la boucle.

Vous ne devez jamais modifier la valeur de la variable de contrôle : l'effet de
ces changements est imprévisible. 

## `for` générique 

La boucle générique pour vous permet de parcourir toutes les valeurs renvoyées
par une fonction d'itération. 

```lua,editable
local a = {"a", "b", "c", "d", "e"}
-- print all values of array `a'
for i,v in ipairs(a) do
    print(i, v)
end
```

```lua,editable
local mapping = {
    ["z"] = "UP",
    ["q"] = "LEFT",
    ["s"] = "DOWN",
    ["d"] = "RIGHT",
}
-- print all values of array `a'
for i,v in pairs(mapping) do
    print(i, v)
end
```

