# `repeat`

Comme son nom l'indique, une instruction `repeat` répète son corps jusqu'à ce
que son état soit vrai. Le test est effectué après le corps, donc le corps est
toujours exécuté au moins une fois.

```lua,editable
local i = 0
local function askUser()
    i = i + 1
    if i > 2 then
        return "OK"
    else
        return nil
    end
end

local res
local try = 0
repeat
    res = askUser()
    try = try + 1
until res or try >= 5

print("res =", res, "on try", try)
```
