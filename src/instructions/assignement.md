# Affectations

L'affectation est le moyen de base de modifier la valeur d'une variable :
```lua,editable
local a = "Salut"
a = "Hello" .. " world"
print(a)
```

Lua autorise plusieurs affectations, où une liste de valeurs est attribuée à
une liste de variables en une seule étape. Les deux listes ont leurs éléments
séparés par des virgules.

```lua,editable
local x = 20
a, b = 10, 2 * x
print(a,b)
```

Dans une affectation multiple, Lua évalue d'abord toutes les valeurs et
n'exécute les affectations qu'après. Par conséquent, nous pouvons utiliser une
affectation multiple pour échanger deux valeurs :

```lua,editable
local x,y = "x", "y"
x, y = y, x -- échanger x et y
print(x, y)

local a = { 0, 1, 2 }
local i,j = 1, 3
a[i], a[j] = a[j], a[i]
print(a[1], a[2], a[3])
```
