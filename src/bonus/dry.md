# DRY

## Don’t Repeat Yourself (ne vous répétez pas)

Le but est d'éviter la duplication de code au sein d’une application afin de
faciliter la maintenance, le test, le débogage et les évolutions de cette
dernière.

Quand on duplique du code, dès qu'on trouve un problème, il faut trouver toutes
les utilisations du code erroné et les corriger.

## Exemple 

Qu'est ce qui pourrait être amélioré dans se code ?

```lua,editable
local topScore = 0

function setScore(score)
    if score < 0 or score > 500 then
        return -- Invalid score
    end

    topScore = score
end

function getScore()
    if score > 0 and score < 500 then
        return topScore
    else
        return nil -- No score
    end
end
```

<details>

```lua,editable
local scoreMax = 500

local function scoreValid(score)
    return score > 0 and score < scoreMax
end

local topScore = 0

local function setScore(score)
    if not scoreValid(score) then
        return
    end

    topScore = score
end

local function getScore()
    if scoreValid(topScore) then
        return topScore
    end
end
```

</details>
