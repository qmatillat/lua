# OOP: Programmation orientée objet

Les tables peuvent porter des operations :
```lua,editable
Compte = {balance = 0}
function Compte.retirer(v)
    Compte.balance = Compte.balance - v
end
```

Avec cette solution, nous sommes limités à une instance de l'objet. Une solution plus flexible serait d'opérer sur une instance de l'objet :
```lua,editable
Compte = {balance = 0}
function Compte.retirer(self, v)
    self.balance = self.balance - v
end

a1 = Compte
a2 = { balance = 0, retirer = Compte.retirer }
a1.retirer(a1, 10) -- équivalent a a1:retirer(10)
print(a1.balance, a2.balance)
a1:retirer(10)
a2:retirer(15)
print(a1.balance, a2.balance)

-- Même syntaxe pour la déclaration, le nom de l'instance est `self`
function Compte:deposer(v)
    self.balance = self.balance + v
end

a1:deposer(5)
print(a1.balance, a2.balance)
```

Pour créer des objets, et éviter d'avoir a copier toutes les fonction (comme
pour `a2`) on peut utiliser les méta-tables :

```lua,editable
Compte = { balance = 0 }
function Compte:new(o)
    o = o or {}   -- crée un objet si aucun fournit
    setmetatable(o, self)
    self.__index = self
    return o
end

function Compte.retirer(self, v)
    self.balance = self.balance - v
end

function Compte:deposer(v)
    self.balance = self.balance + v
end

---
local c1 = Compte:new { balance = 15 }
local c2 = Compte:new() -- balance = 0 comme définit ligne 1

c1:deposer(15)
c1:retirer(5)
print(c1.balance)
```
