# Interface segregation principle

> *S'applique plus aux langages orienté objet*

Aucun client ne devrait dépendre de méthodes qu'il n'utilise pas. Il faut donc
diviser les interfaces volumineuses en plus petites plus spécifiques, de sorte
que les clients n'ait accès qu'aux méthodes qui les intéresse.

Cela permet de maintenir un couplage faible, donc plus facile à refactoriser.
