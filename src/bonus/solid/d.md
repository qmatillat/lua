# Dependency inversion principle

> *S'applique plus aux langages orienté objet*

Ce principe fait référence à une forme spécifique de découplage. En suivant ce
principe, la relation de dépendance conventionnelle que les modules de haut
niveau ont, par rapport aux modules de bas niveau, est inversée dans le but de
rendre les premiers indépendants des seconds.

Les deux assertions de ce principe sont :
- Les modules de haut niveau ne doivent pas dépendre des modules de bas niveau.
  Les deux doivent dépendre d'abstractions.
- Les abstractions ne doivent pas dépendre des détails. Les détails doivent
  dépendre des abstractions.

## Mauvais exemple

```lua,editable
function adder(a, b) return a + b end
function suber(a, b) return a - b end

function calc(a, b, op)
    if op == "+" then
        return adder(a, b)
    elseif op == "-" then
        return suber(a, b)
    else
        error("Unknown operation")
    end
end

print(calc(5, 7, '+'))
```

## Bon exemple
```lua,editable
function adder(a, b) return a + b end
function suber(a, b) return a - b end

local calc = {
    ["+"] = adder,
    ["-"] = suber,
}

function calc:compute(a, b, op)
    local f = self[op] or error("Unknown operation")
    return f(a, b)
end

print(calc:compute(5, 7, "+"))
```
