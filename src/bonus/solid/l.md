# Liskov substitution principle

> *S'applique plus aux langages orienté objet*

Si une propriété s'applique pour tout objet *x* de type *T*, alors elle doit
s'appliquer pour tout objet *y* de type *S*, où *S* est un sous-type de *T*.

## Contrexemple
L'exemple classique d'une violation du principe est le suivant :

- Soit une classe Rectangle représentant les propriétés d'un rectangle :
  hauteur, largeur. 
- Soit une classe Carré que l'on fait dériver de la classe Rectangle, avec
  comme condition `largeur = hauteur`.

```lua,editable
local function rect(larg, haut)
    return {
        largeur = larg,
        hauteur = haut,
    }
end

local function square(size)
    return rect(size, size)
end

local function area(shape)
    return shape.largeur * shape.hauteur
end

-- THIS IS BAD
local function scale(shape, x, y)
    shape.largeur = shape.largeur * x
    shape.hauteur = shape.hauteur * y
end
```

