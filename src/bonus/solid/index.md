# SOLID

L'acronyme SOLID désigne un ensemble de conseil pour produire un code
maintenable


## Principes

- Responsabilité unique (Single responsibility principle)

    Une classe, une fonction ou une méthode doit avoir qu'une et une seule
    responsabilité

- Ouvert/fermé (Open/closed principle)

    Une fonction, un module doit être fermé à la modification directe, mais
    ouverte à l'extension

- Substitution de Liskov (Liskov substitution principle)

    Une instance d'un type doit pouvoir être remplacée par une instance d'un
    sous type sans que cela ne modifie la cohérence du programme

- Ségrégation des interfaces (Interface segregation principle)

    Préférer plusieurs interfaces spécifiques pour chaque client plutôt
    qu'une seule interface générale

- Inversion des dépendances (Dependency inversion principle)

    Il faut dépendre des abstractions, pas des implémentations
