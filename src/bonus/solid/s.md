# Single responsibility principle

Une classe, une fonction ou une méthode doit avoir qu'une et une seule
responsabilité. Elle ne doit changer que pour une seule raison.

## Exemple

Qu'est-ce qui pourrait être amélioré dans ce code ?

```lua,editable
function sendMail(a) print(a) end

function sendFacture(f)
    local msg = "== Facture n°" .. f.num .. " ==\n"
    msg = msg .."- " .. f.qte .. "x " .. f.nom .. " (" .. f.prix .. "€)\n"
    local total = f.qte * f.prix
    msg = msg .. "Total = " .. total .. "€"

    sendMail(msg)
end

sendFacture {
    num = 42,
    qte = 5,
    nom = "Book",
    prix = 49.99,
}
```

<details>

```lua,editable
function sendMail(a) print(a) end

function total(f)
    return f.qte * f.prix
end

function makeFacture(f)
    local msg = "== Facture n°" .. f.num .. " ==\n"
    msg = msg .."- " .. f.qte .. "x " .. f.nom .. " (" .. f.prix .. "€)\n"
    msg = msg .. "Total = " .. total(f) .. "€"

    return msg
end

function printFacture(f)
    print(makeFacture(f))
end

function sendFacture(f)
    sendMail(makeFacture(f))
end

sendFacture {
    num = 42,
    qte = 5,
    nom = "Book",
    prix = 49.99,
}
```

</details>
