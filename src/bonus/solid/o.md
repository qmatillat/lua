# Open/closed principle

Le principe ouvert/fermé affirme qu'une classe doit être à la fois ouverte (à
l'extension) et fermée (à la modification). 

L'idée est de permettre les modifications (ouverture) sans avoir besoin de
changer la classe/méthode (fermeture).

## Exemple

Qu'est ce qui pourrait être amélioré dans se code ?

```lua,editable
function splitOnce(i, sep)
    return i:match("(.-)%".. sep .."(.+)")
end


function somme(d)
    local o = {}
    for k,v in pairs(d) do
        local name, num = splitOnce(k, '_')
        o[name] = (o[name] or 0)  + tonumber(num) * v
    end
    return o
end

local o = somme {
    quentin_1 = 5,
    quentin_2 = 10,

    toto_1 = 4,
    toto_2 = 8,
}

for k,v in pairs(o) do
    print(k, "=", v)
end
```

<details>

```lua,editable
function splitOnce(i, sep)
    return i:match("(.-)%".. sep .."(.+)")
end


function somme(d, split)
    local split = split or function(v) return splitOnce(v, '_') end

    local o = {}
    for k,v in pairs(d) do
        local name, num = split(k)
        o[name] = (o[name] or 0)  + tonumber(num) * v
    end
    return o
end

local o = somme {
    quentin_1 = 5,
    quentin_2 = 10,

    toto_1 = 4,
    toto_2 = 8,
}

for k,v in pairs(o) do
    print(k, "=", v)
end
```

</details>
