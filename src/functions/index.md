# Fonctions

Les fonctions sont le principal mécanisme d'abstraction des déclarations et des
instructions dans Lua. Les fonctions peuvent effectuer une tâche complexe, ou
un simple calcule :

```lua,editable
print(8*9, 9/8)
a = math.sin(3) + math.cos(10)
print(os.date())
```

Nous écrivons une liste d'arguments enfermés entre parenthèses. Si l'appel de
fonction n'a aucun argument, nous devons écrire une liste vide `()` pour
indiquer l'appel. Il y a un cas particulier à cette règle : si la fonction a un
seul argument et que cet argument est soit une chaîne littérale ou un
constructeur de table, les parenthèses sont facultatives :

```lua,editable
local function sum(list)
    local t = 0
    for _,v in ipairs(list) do
        t = t + v
    end
    return t
end
print "Hello World"
print [[a multi-line
 message]]
print( sum{1, 2, 3} )
print( type{} )
```

Lua propose également un sucre syntaxique pour les appels orientés objet,
l'opérateur `:`. Une expression comme `o:foo(x)` est juste une autre façon
d'écrire `o.foo(o, x)`.

