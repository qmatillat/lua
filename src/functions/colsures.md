# Fermeture / Clôture (Closure en anglais)

Lorsqu'une fonction est écrite enfermée dans une autre fonction, elle a un
accès complet aux variables locales

```lua,editable
function newCounter()
    local i = 0
    return function() -- i est "capturé" par la fonction anonyme
        i = i + 1
        return i
    end
end

c1 = newCounter()
print(c1())
print(c1())

c2 = newCounter() -- Deuxième compteur indépendant
print(c2())
print(c1())
print(c2())
```

Comme les fonctions sont stockées dans des variables classique, nous pouvons
facilement redéfinir des fonctions dans Lua, même des fonctions prédéfinies.
Fréquemment, lorsque vous redéfinissez une fonction, vous avez
besoin de la fonction d'origine dans la nouvelle implémentation. Par exemple,
supposons que vous souhaitiez redéfinir la fonction `sin` pour fonctionner en
degrés au lieu de radians. Cette nouvelle fonction doit convertir son argument,
puis appeler la fonction `sin` d'origine pour faire le vrai travail :

```lua,editable
oldSin = math.sin
math.sin = function (x)
    return oldSin(x*math.pi/180)
end
```

La version plus propre serait de faire :

```lua,editable
do -- bloc privée pour eviter de créer un variable dans le bloc de base
    local oldSin = math.sin
    local k = math.pi/180 -- constante de conversion deg -> rad
    math.sin = function (x)
        return oldSin(x*k)
    end
end
```
