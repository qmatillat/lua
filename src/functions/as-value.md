# Fonction comme valeurs

Les fonctions dans LUA sont des valeurs de **première classe** avec une
**portée lexicale** appropriée.

#### Valeurs de première classe
Une fonction est une valeur avec les mêmes propriétés que des valeurs
conventionnelles (comme les nombres, les chaînes ...). Les fonctions peuvent être
stockées en variables (globales et locales) et dans les tableaux, peuvent être
transmises sous forme d'arguments et peuvent être renvoyées par d'autres
fonctions.

#### Portée lexicale
Les fonctions peuvent accéder aux variables des blocs parents.

## Nommage
Les fonctions, comme toutes les autres valeurs, sont anonymes. Lorsque nous
parlons d'un nom de fonction (`print` par exemple), nous parlons en fait d'une
variable qui contient cette fonction. Comme toute autre variable contenant
n'importe quelle autre valeur, nous pouvons manipuler ces variables :

```lua,editable
a = {p = print}
a.p("Hello World")
print = math.sin
a.p(print(1))
sin = a.p
sin(10, 20)
```

Techniquement, la syntaxe de déclaration que l'on a vue, est un sucre syntaxique :
```lua,editable
function foo (x) return 2*x end
-- est simplement
foo = function (x) return 2*x end
```

Comme une fonction est une valeur, on peut aussi la passer a d'autres fonctions :
```lua,editable
local songs = {
    {name = "grauna",  duration = 2},
    {name = "arraial", duration = 7},
    {name = "lua",     duration = 12},
    {name = "derain",  duration = 4},
}

print("=== Sort by name ===")
table.sort(songs, function (a,b)
    return (a.name < b.name)
end)

for _,v in ipairs(songs) do
    print(v.name, v.duration)
end

print("=== Sort by duration ===")
table.sort(songs, function (a,b) return a.duration < b.duration end)
for _,v in ipairs(songs) do
    print(v.name, v.duration)
end
```

