# Résultats

Une caractéristique non conventionnelle, mais assez pratique de Lua est que les
fonctions peuvent renvoyer plusieurs résultats. Un exemple est la fonction
`string.find`, qui checher un motif dans une chaîne. Il renvoie deux indices :
l'indice du caractère où le motif est trouvé et celui où il se termine

```lua,editable
s, e = string.find("Hello Lua Users", "Lua")
print(s, e)
```

Les fonctions que l'on définit peuvent également renvoyer plusieurs résultats,
en les répertoriant tous après le mot clé de retour. Par exemple, une fonction
pour trouver l'élément maximum dans un tableau peut renvoyer à la fois la
valeur maximale et son emplacement :

```lua,editable
function maximum (a)
    local mi              -- maximum index, default to nil
    local m = -math.huge  -- maximum value, default to -Infinity
    for i,val in ipairs(a) do
        if val > m then
            mi = i
            m = val
        end
    end
    return m, mi
end

print(maximum({8,10,23,12,5}))     --> 23   3
```

