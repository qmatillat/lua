# Arguments nommés

Le mécanisme de passage des paramètres dans LUA est positionnel : lorsque nous
appelons une fonction, les arguments correspondent aux paramètres par leurs
positions.

Dans certains cas, il est pratique d'avoir des arguments nommés (pour clarifier
le code par exemple) :

```lua
Button(
    width = 300,
    height = 200,
    title = "Lua",
    background = "blue",
    border = true,
    on_click = ...
)
```

Lua n'a pas de support pour cette syntaxe, mais nous pouvons avoir le même
effet. L'idée ici est de mettre tous les arguments dans une table et d'utiliser
cette table comme le seul argument de la fonction. En plus, avec la syntaxe
spéciale de Lua pour les appels de fonction avec un seul paramètre, on arrive à
un résultat proche de l'extrait de code ci-dessus :

```lua,editable
local function Button(options)
    -- check mandatory options
    if type(options.title) ~= "string" then
        error("no title")
    elseif type(options.width) ~= "number" then
        error("no width")
    elseif type(options.height) ~= "number" then
        error("no height")
    end

    -- everything else is optional
    print("Button",
        options.title,
        options.x or 0,    -- default value
        options.y or 0,    -- default value
        options.width,
        options.height,
        options.background or "white",   -- default
        options.border       -- default is false (nil)
    )

    if options.on_click then
        options.on_click()
    end
end

local btn = Button {
    width = 300,
    height = 200,
    title = "Lua",
    background="blue",
    border = true,
    on_click = function()
        print("Clicked")
    end
}
```
