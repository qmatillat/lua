# Récapitulatif

```lua,editable
local function maFonction(argument)
    -- Commentaire
end

local number = 10
for i = 0, number - 1 do
    print(i)
end

local id = -1
if id == -1 then
    id = 5
end
print(id)

local t = {}
table.insert(t, 1.0)
t[2] = 2.0
table.remove(t, 2)

for s = 0.3, 1.2, 0.2 do
    print(s)
end
```
