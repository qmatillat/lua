# Chercher dans la doc

La documentation officielle est sous la forme d'un livre accessible
gratuitement sur le site officiel de lua : [Programming in Lua]

Il y a aussi une documentation (très technique) mais qui contient le détail des
fonctions disponibles de base : [Manual]

On peut aussi trouver des exemples semi-officiels sur [lua-users]: [Sample
Code] ou une référence en une page: [Short Reference]

[Programming in Lua]: https://www.lua.org/pil/contents.html
[Manual]: https://www.lua.org/manual/5.4/
[Sample Code]: http://lua-users.org/wiki/SampleCode
[lua-users]: http://lua-users.org/wiki/LuaDirectory
[Short Reference]: http://thomaslauer.com/download/luarefv51.pdf
