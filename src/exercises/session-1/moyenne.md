# Moyenne

## Énoncé

Implémenter une fonction qui calcule une somme, une moyenne, une moyenne
groupée...

## Code de base

```lua,editable
local function sum(x)
    return 0
end

local function avg(x)
  return 0
end

local function group_avg(lst)
    return {}
end


local function test(got, expected)
    if expected ~= got
    then
        print("Expected", expected, "Got", got)
    else
        print("OK", got)
    end
end

test(sum{1, 2, 3}, 6)
test(sum{}, 0)
test(sum{8}, 8)
test(sum{8, 4, -7, 0}, 5)

test(avg{1, 2, 3}, 2)
test(avg{8}, 8)
test(avg{8, 4, -7, 0}, 1.25)

local res = group_avg {
    { name = "Quentin", val = 15 },
    { name = "Toto", val = 11 },
    { name = "Quentin", val = 13 },
}
test(res.Quentin, (15 + 13) / 2)
test(res.Toto, 11)
```

## Solution

<details>

```lua,editable
local function sum(x)
  local s = 0
  for _, v in ipairs(x) do s = s + v end
  return s
end

local function avg(x)
  return sum(x) / #x
end

local function group_avg(lst)
    local ret = {}
    local tot = {}
    for _, v in pairs(lst) do
       ret[v.name] = (ret[v.name] or 0) + v.val
       tot[v.name] = (tot[v.name] or 0) + 1
    end

    for n, v in pairs(ret) do
       ret[n] = v / tot[n]
    end
    return ret
end


local function test(got, expected)
    if expected ~= got
    then
        print("Expected", expected, "Got", got)
    else
        print("OK", got)
    end
end

test(sum{1, 2, 3}, 6)
test(sum{}, 0)
test(sum{8}, 8)
test(sum{8, 4, -7, 0}, 5)

test(avg{1, 2, 3}, 2)
test(avg{8}, 8)
test(avg{8, 4, -7, 0}, 1.25)

local res = group_avg {
    { name = "Quentin", val = 15 },
    { name = "Toto", val = 11 },
    { name = "Quentin", val = 13 },
}
test(res.Quentin, (15 + 13) / 2)
test(res.Toto, 11)
```

</detail>
