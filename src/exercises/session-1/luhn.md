# Algorithme de Luhn

L'[algorithme de Luhn](https://fr.wikipedia.org/wiki/Formule_de_Luhn) est
notamment utilisé pour valider les numéros de cartes de crédits. L'algorithme
prend une chaîne comme entrée et appliques les étapes suivantes pour valider le
numéro :

- Ignorer tous les espaces. Refuser les nombres avec moins de 2 chiffres.
- De droite à gauche, doubler un chiffre sur deux : par exemple `1234`, on
  double `3` et `1`
- Après avoir doublé chaque chiffre, calculer la somme de chiffre. Doubler `7`
  deviens `14` qui devient `5`
- Sommer tous les chiffres
- Le code de carte est valide si la somme se termine avec un `0`.

```lua,editable
local function luhn(number)
    return false, "Not implemented yet"
end

local function test(cc, expected)
    local res, err = luhn(cc)
    if expected
    then
        if not res
        then
            print("Failed to validate valid card number " .. cc, err)
        else
            print("OK [  valid] " .. cc)
        end
    else
        if res
        then
            print("Accepted invalid card number " .. cc)
        else
            print("OK [invalid] " .. cc)
        end
    end
end

test("foo", false)
test("", false)
test(" ", false)
test("  ", false)
test("    ", false)
test("0", false)
test(" 0 0 ", true)
test("4263 9826 4026 9299", true)
test("4539 3195 0343 6467", true)
test("7992 7398 713", true)
test("4223 9826 4026 9299", false)
test("4539 3195 0343 6476", false)
test("8273 1232 7352 0569", false)
```

<details>

```lua,editable
local function luhn(number)
    local handled = 0
    local sum = 0

    for i = #number, 1, -1 do
        local char = number:sub(i,i)
        if char ~= " " then
            local v = tonumber(char)
            if not v then 
                return false, "<" .. char .. "> is not a digit"
            end

            handled = handled + 1
            if handled % 2 == 0
            then
                v = v * 2
                if v > 10 then -- *2 can be at most 18
                    v = v - 9 -- -10 + 1
                end
            end
            sum = sum + v
        end
    end

    if handled < 2 then
        return false, "Need more than 2 char"
    end

    if sum % 10 ~= 0 then
        return false, "Invalid sum digit: " .. sum
    end

    return true
end

local function test(cc, expected)
    local res, err = luhn(cc)
    if expected
    then
        if not res
        then
            print("Failed to validate valid card number " .. cc, err)
        else
            print("OK [  valid] " .. cc)
        end
    else
        if res
        then
            print("Accepted invalid card number " .. cc)
        else
            print("OK [invalid] " .. cc)
        end
    end
end

test("foo", false)
test("", false)
test(" ", false)
test("  ", false)
test("    ", false)
test("0", false)
test(" 0 0 ", true)
test("4263 9826 4026 9299", true)
test("4539 3195 0343 6467", true)
test("7992 7398 713", true)
test("4223 9826 4026 9299", false)
test("4539 3195 0343 6476", false)
test("8273 1232 7352 0569", false)
```

</details>
