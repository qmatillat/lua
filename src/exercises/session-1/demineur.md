# Démineur

A partir d'une carte de démineur (tableau a 2 dimensions de caractère),
remplisser le nombre de bombe adjacante de chaque cellules

```lua,editable
local base = {
    {"·", "*", "·", "*", "·"},
    {"·", "·", "*", "·", "·"},
    {"·", "·", "*", "·", "·"},
    {"·", "·", "·", "·", "·"},
}

local result = {
    { 1 , "*",  3 , "*",  1 },
    { 1 ,  3 , "*",  3 ,  1 },
    {"·",  2 , "*",  2 , "·"},
    {"·",  1 ,  1 ,  1 , "·"},
}

function makeGrid(grid)
    -- TODO
end

makeGrid(base)
for _, row in pairs(base) do
    print(row)
end

-- Check result
for i,row in ipairs(base) do
    for j,cell in ipairs(row) do
        if cell ~= result[i][j] then
            print("Difference at cell " .. i .. "," .. j)
            print("\tExpected\t" .. result[i][j])
            print("\tComputed\t" .. cell)
            print()
        end
    end
end
```

<details>

```lua,editable
local base = {
    {"·", "*", "·", "*", "·"},
    {"·", "·", "*", "·", "·"},
    {"·", "·", "*", "·", "·"},
    {"·", "·", "·", "·", "·"},
}

local result = {
    { 1 , "*",  3 , "*",  1 },
    { 1 ,  3 , "*",  3 ,  1 },
    {"·",  2 , "*",  2 , "·"},
    {"·",  1 ,  1 ,  1 , "·"},
}

function makeGrid(grid)
    local function increase(x, y)
        if grid[x] == nil or grid[x][y] == nil then
            return -- Out of grid
        end

        if grid[x][y] == "*" then
            return -- Don't change bomb
        end

        if grid[x][y] == "·" then
            grid[x][y] = 1
            return
        end

        grid[x][y] = grid[x][y] + 1
    end

    local function increaseNeighboor(x, y)
        for dx = -1,1 do
            for dy = -1,1 do
                increase(x + dx, y + dy)
            end
        end
    end

    for i,row in ipairs(base) do
        for j,cell in ipairs(row) do
            if cell == "*" then
                increaseNeighboor(i, j)
            end
        end
    end
end

makeGrid(base)
for _, row in pairs(base) do
    print(row)
end

-- Check result
for i,row in ipairs(base) do
    for j,cell in ipairs(row) do
        if cell ~= result[i][j] then
            print("Difference at cell " .. i .. "," .. j)
            print("\tExpected\t" .. result[i][j])
            print("\tComputed\t" .. cell)
            print()
        end
    end
end
```

</details>
