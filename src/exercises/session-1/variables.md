# Exercices

## Énoncé

À partir d'un âge (en secondes) et le nom d'une planète, calculer l'age que la
personne aurait sur cette planète.

| Planète | Période orbitale par rapport à celle de la terre |
|---------|--------------------------------------------------|
| Mercure | 0.2408467                                        |
| Venus   | 0.61519726                                       |
| Terre   | 1.0 (365.25 jours, 31 557 600 seconds)           |
| Mars    | 1.8808158                                        |
| Jupiter | 11.862615                                        |
| Saturn  | 29.447498                                        |
| Uranus  | 84.016846                                        |
| Neptune | 164.79132                                        |

Donc, si on vous a dit que quelqu'un avait 1 000 000 000 secondes, vous devriez
être en mesure de dire qu'il a 31,69 ans de terre.

> Remarque : La longueur réelle d'une orbite complète de la Terre autour du
> soleil est plus proche de 365,256 jours. Le calendrier
> grégorien a, en moyenne, 365,2425 jours. Bien qu'il ne soit pas entièrement
> précis, 365.25 est la valeur utilisée dans cet exercice. Voir l'[année (sur
> Wikipédia)](https://fr.wikipedia.org/wiki/Ann%C3%A9e_(calendrier)#Les_diff%C3%A9rentes_ann%C3%A9es_classiques)
> pour plus de façons de mesurer l'année.

## Code de base
```lua,editable
local function age(ageSec, planete)
    -- TODO: Ecrire la fonction
    return 0
end

local function test(got, expected)
    local function round(v, digit)
        local offset = 10 ^ digit
        return math.floor((v * offset) + 0.5) / offset
    end

    local got = round(got, 2)
    if expected ~= got
    then
        print("Expected", expected, "Got", got)
    else
        print("OK", got)
    end
end

test(age(1000000000, "Terre"), 31.69)
test(age(1000000000, "Venus"), 51.51)
test(age(1000000000, "Uranus"), 0.38)
test(age(797558400, "Mars"), 13.44)
```

## Solution

<details>

```lua,editable
local Planete = {
    Mercure = 0.2408467;
    Venus   = 0.61519726;
    Terre   = 1.0;
    Mars    = 1.8808158;
    Jupiter = 11.862615;
    Saturn  = 29.447498;
    Uranus  = 84.016846;
    Neptune = 164.79132;
}

local function age(ageSec, planete)
    return ageSec / 31557600 / Planete[planete]
end

local function test(got, expected)
    local function round(v, digit)
        local offset = 10 ^ digit
        return math.floor((v * offset) + 0.5) / offset
    end

    local got = round(got, 2)
    if expected ~= got
    then
        print("Expected", expected, "Got", got)
    else
        print("OK", got)
    end
end

test(age(1000000000, "Terre"), 31.69)
test(age(1000000000, "Venus"), 51.51)
test(age(1000000000, "Uranus"), 0.38)
test(age(797558400, "Mars"), 13.44)
```

</details>
