# Exercice Fizz-Buzz

## Énoncé

Le test Fizz-Buzz est issu d'une méthode permettant d'apprendre les divisions
aux enfants.

Pour chaque nombre de 1 à `n` :
- Si le nombre est divisible par 3 : on écrit **fizz**.
- Si le nombre est divisible par 5 : on écrit **buzz**.
- Si le nombre est divisible par 3 et par 5 : on écrit **fizzbuzz**.
- Sinon : on écrit le nombre.

Exemple de 1 à 15 :
```
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
```

## Code de base
```lua,editable
function is_divisible(n, divisor)
end

function fizzbuzz(n)
end

function print_fizzbuzz_to(n)
    for i = 1,n
    do
        print(fizzbuzz(i))
    end
end

print_fizzbuzz_to(20)
```

<details>

```lua,editable
function is_divisible(n, divisor)
    if divisor == 0
    then
        return false
    end

    return n % divisor == 0
end

function fizzbuzz(n)
    local fizz = ""
    local buzz = ""
    if is_divisible(n, 3) then fizz = "fizz" end
    if is_divisible(n, 5) then buzz = "buzz" end

    local out = fizz .. buzz
    if out == ""
    then
        out = tostring(n)
    end

    return out
end

function print_fizzbuzz_to(n)
    for i = 1,n
    do
        print(fizzbuzz(i))
    end
end

print_fizzbuzz_to(20)
```
</details>
