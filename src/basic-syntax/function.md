# Fonctions

Les fonctions sont des valeurs en Lua.

Cela signifie que les fonctions peuvent être stockées dans des variables, transmises en arguments à d'autres fonctions et renvoyées comme résultats.

```lua,editable
function logger(prefix)
    prefix = string.format("%10s", prefix)
    return function(val)
        print(prefix .. ": " .. val)
    end
end

function count_to(n, f)
    for i = 1,n
    do
        f(i)
    end
end

local log = logger("Boucle 1")
local log2 = logger("Boucle 2")
local main_log = logger("Main")

main_log("Prêt ?")
count_to(10, log)
main_log("Terminé, multiple de 2:")
count_to(10, function (v)
    log2(v * 2)
end)
print("Raw print call")
```

