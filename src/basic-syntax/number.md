# Nombres

Le type `number` représente les nombres réels.

On peut écrire des nombres avec une partie décimale en option, plus un exposant décimal facultatif. 

```lua,editable
print(4, 0.4, 4.57e-3, 0.3e12)
print(5e+20, 5e+40, math.huge)
```
