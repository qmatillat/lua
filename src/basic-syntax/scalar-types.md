# Types de base

Lua est un langage dynamiquement typé. Il n'y a pas de définition de type dans
le langage. Chaque valeur porte son propre type.

Il y a 5 types en Lua :


| Représente          | Types      | Littéral         |
|---------------------|------------|------------------|
| Rien                | `nil`      | `nil`            |
| Nombres             | `number`   | `0`, `1`, `3.14` |
| Booléen             | `boolean`  | `true`, `false`  |
| Chaîne de caractère | `string`   | `""`, `"Chaîne"` |
| Fonction            | `function` | `function() end` |

On peut retrouver le type d'un object avec la fonction `type` :
```lua,editable
print(type("Hello world"))
print(type(10.4*3))
print(type(print))
print(type(type))
print(type(true))
print(type(nil))
print(type(type(X)))
```

Les variables n'ont pas de type prédéfinit, elles peuvent en changer durant
leur vie :

```lua,editable
local a
print(type(a))
a = 10
print(type(a))
a = "a string!!"
print(type(a))
a = print
a(type(a))
```
