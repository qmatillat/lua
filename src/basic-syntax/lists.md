# Liste

```lua,editable
local reseaux = {"Facebook", "Instagram", "Snapchat", "Twitter"}
print(reseaux)

reseaux[4] = "X"
print(reseaux)

table.insert(reseaux, "TikTok") -- ou reseaux[#reseaux + 1] = "TikTok"
print(reseaux)

table.remove(reseaux, 2)
print(reseaux)

table.sort(reseaux)
print(reseaux)
```

