# `Nil`

`Nil` est un type avec une seule value, `nil`, dont la propriété principale est
d'être différent de toute autre valeur.

Une variable non initialisée à une valeur `nil` par défaut

**Lua utilise `nil` comme une sorte de non-valeur, pour représenter l'absence
d'une valeur utile.**

```lua,editable
-- `age` est optionnel
local function saluer(nom, age)
    local message = "Bonjour " .. nom
    if age ~= nil
    then
        message = message .. ", vous avec " .. age .. " ans"
    end

    print(message)
end

saluer("Quentin", 26)
saluer("Toto")
```
