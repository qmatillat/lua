# Chaîne de caractère

Les chaînes de caractères sont délimitées par `"` ou `'`.

On peut échapper certains caractères :

Code | Résultat
-----|------------------
`\n` | Retour a la ligne
`\t` | Tabulation
`\\` | Anti-slash
`\"` | Guillemet double
`\'` | Guillemet simple

Il existe des fonctions de manipulation de chaine de caractère dans la
librairie `string`, le plus utilisées sont :
- `find` : Retourne l'emplacement d'une autre chaîne.
- `format` : Formate une chaîne de caractères.
- `match` / `gmatch` : Retourne une / les sous-chaînes respectant un schéma.
- `sub` : Retourne la sous chaîne entre les indices début / fin.
- `gsub` : Remplace toutes les occurrences d'une sous-chaîne.
- `len` : Retourne la longueur d'une chaîne de caractères (en octet, attention
  aux caractères spéciaux).

```lua,editable
a = "Une chaîne"
b = a:gsub('Une', 'Autre') -- remplace Une par Autre
print(a)
print(b)

print(string.format("pi = %.4f", math.pi))
```

On peut aussi définir des chaînes brutes avec `[[ ... ]]`. Dans ce cas les
caractères d'échappement ne marchent pas.

```lua,editable
print([[
Longue
Chaîne
De
Caractères
]])
```

Lua propose aussi des conversions implicites entre nombre et chaine de
caractères *(il existe aussi `tonumber` et `tostring`)*

```lua,editable
print("10" + 1)
print("10 + 1")
print("-5.3e-10"*"2")
-- print("hello" + 1)
```

L'opération de concaténation en Lua est `..`.

```lua,editable
print("Hello" .. " " .. "World")
local name = "Quentin"
print("Hello " .. name .. "!")
print(10 .. 20)
```
