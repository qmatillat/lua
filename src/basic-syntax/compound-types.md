# Types composites

Le type `table` implémente des tableaux associatifs. Un tableau associatif est
un tableau qui peut être indexé non seulement avec des nombres (commençant à
1), mais aussi avec des chaînes ou toute autre valeur du langage, sauf `nil`. 

Les `table` sont le **seul** mécanisme de structuration des données en Lua.

```lua,editable
a = {}              -- crée une table
k = "x"
a[k] = 10           -- nouvelle entrée, clé: "x", valuer: 10
a[20] = "val"       -- nouvelle entrée, clé: 20, valeur: "val"
print(a["x"])

k = 20
print("a[k = " .. k .. "] = " .. a[k])
a["x"] = a["x"] + 1 -- Ajoute 1 à la clé "x"
print(a["x"])
```

Création d'une table directement avec des valeurs :

```lua,editable
local user = {
    age = 25,
    prenom = "Quentin",
}
print(user.prenom .. " a " .. user.age .. " ans")

local p = "prenom"
print(user[p])
```
