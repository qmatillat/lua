# Booléen

Le type `boolean` à deux valeurs, `false` et `true`

En Lua, n'importe quelle valeur peut représenter une condition : on considère
`false` et `nil` comme faux et tout le reste comme vrai. 

```lua,editable
if false    then print("NEVER") end
if nil      then print("NEVER") end

if true     then print("OK") end
if ""       then print("OK") end
if 0        then print("OK") end
if print    then print("OK") end
```
