# Les variables

## Qu’est-ce qu’une variable ?

Une variable est composée de 3 éléments :
- Un nom
- Un type
- Une valeur

Les variables permettent de stocker une information temporairement (choix d'un
utilisateur, configuration ...). Elle peut changer de valeur pendant
l'exécution du programme.

## Créer et modifier des variables

```lua,editable
local age = 5
local nom = "Quentin"
print(nom, age)
nom = "MATILLAT"
print(nom, age)
```
