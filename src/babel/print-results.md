# Afficher les résultats

**Objectifs :** exporter les résultats dans une forme exploitable

2 versions :
- Utiliser les logs de Babel
- Écrire un CSV

<details>

```lua
---@alias Result table<string, table<string, number>>

---@param mission_class_count Result
function PrintLua(mission_class_count)
    print("local result = {")
    for mission, class_count in pairs(mission_class_count) do
        print('\t["' .. mission .. '"] = {')
        for class, num in pairs(class_count) do
            print("\t\t" .. class .. " = " .. num .. ",")
        end
        print('\t},')
    end
    print("}")
end

---@param mission_class_count Result
---@param file? file*
function WriteCsv(mission_class_count, file)
    local file = file or io.open("C:\\Temp\\class.csv", "w+")
    if not file then
        error("File not found")
    end

    file:write("Mission;Class;Count\n")
    for mission, class_count in pairs(mission_class_count) do
        for class, num in pairs(class_count) do
            file:write(mission, ";", class, ";", num, "\n")
        end
    end

    file:close()
end
```

</details>
