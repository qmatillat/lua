local db = Babel:IScenaricDatabase()

---@alias PropChecker fun(val: string): boolean

---@class SearchOptions
---@field class? string[]
---@field includeFolder? boolean
---@field property? table<string, PropChecker>

---@param source UniqueId[]
---@param fn fun(arch: ScenaricArchetype)
---@param filter? SearchOptions
function ForEachChild(source, fn, filter)
    filter = filter or {}

    for _, rootId in ipairs(source)
    do
        local root = db:GetEntityOrArchetype(rootId)
        if Filter(root, filter) then
            local child = root:GetInheritedArchetypes()
            if #child == 0 or filter.includeFolder then
                fn(root)
            end

            ForEachChild(child, fn, filter)
        end
    end
end

---@param id UniqueId
---@param fn fun(arch: ScenaricArchetype)
---@param filter SearchOptions
function ForEachInZone(id, fn, filter)
    db:LoadZone(id, false, false)

    local entites = db:GetEntitiesByLocation(EZoneLocation, id)
    for _, entityId in ipairs(entites)
    do
        local entity = db:GetEntityOrArchetype(entityId)
        if Filter(entity, filter) then
            fn(entity)
        end
    end

    db:UnloadZone(id, false)
end

---@generic T
---@param needle T
---@param haystack T[]
---@return number?
local function SearchArray(needle, haystack)
    for k, v in ipairs(haystack)
    do
        if v == needle then
            return k
        end
    end
end

---@param arch ScenaricArchetype
---@param search SearchOptions
---@return boolean
function Filter(arch, search)
    if search.class then
        if not SearchArray(arch:GetClassName(), search.class) then
            return false
        end
    end

    if search.property then
        for k, v in pairs(search.property) do
            if not arch:DoesMemberExists(k) then
                return false
            end

            if not v(arch:GetMemberValue(k)) then
                return false
            end
        end
    end

    return true
end

---@param mission_class_count table<string, table<string, number>>
function PrintLua(mission_class_count)
    print("local result = {")
    for mission, class_count in pairs(mission_class_count) do
        print('\t["' .. mission .. '"] = {')
        for class, num in pairs(class_count) do
            print("\t\t" .. class .. " = " .. num .. ",")
        end
        print('\t},')
    end
    print("}")
end

---@param mission_class_count table<string, table<string, number>>
---@param file? file*
function WriteCsv(mission_class_count, file)
    local file = file or io.open("C:\\Temp\\class.csv", "w+")
    if not file then
        error("File not found")
    end

    file:write("Mission;Class;Count\n")
    for mission, class_count in pairs(mission_class_count) do
        for class, num in pairs(class_count) do
            file:write(mission, ";", class, ";", num, "\n")
        end
    end

    file:close()
end

-- local zoneId = db:GetZoneByName("ORL_MAIN_EVENT_SC_DF_004_Uid5200000000006057_S02")

---@type table<string, table<string, number>>
local mission_class_count = {}

local missions = GetAllMission()
for _, v in ipairs(missions) do
    if v.name:find("SR") then
        local class_count = {}
        ---@param arch ScenaricArchetype
        local function ClassCouter(arch)
            local class = arch:GetClassName()
            class_count[class] = (class_count[class] or 0) + 1
        end

        ForEachInZone(v.zone, ClassCouter, {
            property = {
                ParticleFX = function(id) return UniqueId:FromStringValue(id):IsValid() end
            }
        })

        mission_class_count[v.name] = class_count
    end
end

WriteCsv(mission_class_count)
