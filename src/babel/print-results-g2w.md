# Utiliser le helper G2W

**Objectifs**: simplifier l'affichage avec un helper `CSV` existant

### Exemple d'utilisation

```lua
local CSV = require "G2W.csv"
local csv = CSV:new { "A", "B", "C" }
csv:add_row { A = 7, B = 0, C = "Test" }
print(csv:to_string())
-- csv:write_to(file)
```

<details>

```lua
---@alias Result table<string, table<string, number>>
---@param mission_class_count Result
---@param file? file*
function WriteCsv(mission_class_count, file)
    local file = file or io.open("C:\\Temp\\class.csv", "w+")
    if not file then
        error("File not found")
    end

    local csv = CSV:new { "Mission", "Class", "Count" }
    for mission, class_count in pairs(mission_class_count) do
        for class, num in pairs(class_count) do
            csv:add_row{ Mission = mission, Class = class, Count = num }
        end
    end

    csw:write_to(file)
    file:close()
end
```

</details>
