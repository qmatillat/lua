# Scripter dans Babel

## Shelve

Dans Babel, on peut lancer un script depuis plusieurs endroits:

![Shelve](./shelve.png)

Les réglages globaux sont faits depuis le `C++`, mais chaque utilisateur peut
créer sa propre configuration. On peut aussi l'exporter dans un fichier pour la
partager.

![Add to shelve](./addShelve.png)

## Menu contextuel

On peut aussi ajouter un script dans un menu contextuel en fonction du type de l'objet.

![Add to context menu](./contextMenu.png)


## Script Editor

On a aussi accès à un éditeur de script intégré à Babel, on peut y lancer des
scripts, les exécuter en mode pas à pas, inspecter des variables...

![ScriptEditor](./scriptEditor.png)
