# Option pour inclure les archétypes intermédiaires

Objectifs : Ajouter a la structure un champ qui permet d'inclure les archétypes
intermédiaires lors de l'itération.

<details>

```lua
---@class SearchOptions
---@field class? string[]
---@field includeFolder? boolean

---@param source UniqueId[]
---@param fn fun(arch: ScenaricArchetype)
---@param search? SearchOptions
function ForEachChild(source, fn, search)
    for _, rootId in ipairs(source)
    do
        local root = db:GetEntityOrArchetype(rootId)
        local child = root:GetInheritedArchetypes()
        if (#child == 0 or search.includeFolder) and Filter(child, search) then
            fn(root)
        end

        ForEachChild(child, fn, search)
    end
end
```

</details>
