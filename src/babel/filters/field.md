# Filtrer en fonction de la valeur d’un champ

**Objectifs :** Ajouter la possibilité de tester l'existence d'un ou
plusieurs champs, avec vérification optionnelle d'une valeur.

<details>

```lua
---@alias PropChecker fun(val: string): boolean

---@class SearchOptions
---@field class? string[]
---@field includeFolder? boolean
---@field property? table<string, PropChecker>

---@param arch ScenaricArchetype
---@param search SearchOptions
---@return boolean
function Filter(arch, search)
    if search.class then
        if not SearchArray(arch:GetClassName(), search.class) then
            return false
        end
    end

    if search.property then
        for k, v in pairs(search.property) do
            if not arch:DoesMemberExists(k) then
                return false
            end

            if not v(arch:GetMemberValue(k)) then
                return false
            end
        end
    end

    return true
end

---@return PropChecker
function Exist()
    return function() return true end
end

---@return PropChecker
function Match(expected)
    return function(v) return v == expected end
end

local checker = {
    property = {
        ParticleFX = Exist(),
        DebugDraw2 = Match("0"),
    }
}
```

</details>
