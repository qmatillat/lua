# Filtrer en fonction de la classe

**Objectifs :** créer une structure et une fonction qui filtre un
`ScenaricArchetype` en fonction de sa classe.

<details>

```lua
---@class SearchOptions
---@field class? string[]

---@generic T
---@param needle T
---@param haystack T[]
---@return number?
local function SearchArray(needle, haystack)
    for k, v in ipairs(haystack)
    do
        if v == needle then
            return k
        end
    end
end

---@param arch ScenaricArchetype
---@param search SearchOptions
---@return boolean
function Filter(arch, search)
    if search.class then
        if not SearchArray(arch:GetClassName(), search.class) then
            return false
        end
    end

    return true
end


---@param source UniqueId[]
---@param fn fun(arch: ScenaricArchetype)
---@param search? SearchOptions
function ForEachChild(source, fn, search)
    for _, rootId in ipairs(source)
    do
        local root = db:GetEntityOrArchetype(rootId)
        local child = root:GetInheritedArchetypes()
        if #child == 0 and Filter(child, search) then
            fn(root)
        end

        ForEachChild(child, fn, search)
    end
end
```

</details>
