# Convertir en classe

[Exemple de classes](/bonus/oop.html)

### Exemple d'API utilisateur

```lua
local so = SearchOptions:new()
so:AllowClass("CITEntityParticleFx")
so:PropertyExist("ParticleFX")
so:PropertyValue("DebugDraw2", "1")

ForEachChild(id, fn, so)
```


<details>

```lua
---@class SearchOptions
---@field class? string[]
---@field includeFolder? boolean
---@field property? table<string, PropChecker>
local SearchOptions = {}


---@param o? SearchOptions
---@return SearchOptions
function SearchOptions:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

---@param arch ScenaricArchetype
---@return boolean
function SearchOptions:Check(arch)
    if self.class then
        if not SearchArray(arch:GetClassName(), self.class) then
            return false
        end
    end

    if self.property then
        for k, v in pairs(self.property) do
            if not arch:DoesMemberExists(k) then
                return false
            end

            if not v(arch:GetMemberValue(k)) then
                return false
            end
        end
    end

    return true
end

---@param c string
function SearchOptions:AllowClass(c)
    if self.class then
        table.insert(self.class, c)
    else
        self.class = { c }
    end
end

---@param path string
---@param checker PropChecker
function SearchOptions:CheckProperty(path, checker)
    if not self.property then
        self.property = {}
    end

    self.property[path] = checker
end

---@param path string
function SearchOptions:PropertyExist(path)
    self:CheckProperty(path, Exist())
end

---@param path string
---@param val string
function SearchOptions:PropertyValue(path, val)
    self:CheckProperty(path, Match(val))
end
```

</details>
