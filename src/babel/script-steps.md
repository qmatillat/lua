# Étapes du script

<details>

- (Dé)chargement d'une zone / secteur
- Itération sur un les archétypes d'une zone / secteur
- Afficher les résultats
- Système de filtre

**Conseil :** commencer le fichier avec :
```lua
local db = Babel:IScenaricDatabase()
```

</details>
