# Script complet

**Objectifs :** Pour chaque mission / secteur, compter le nombre d'instances de
chaque classe

> On peut filtrer uniquement les missions qui contiennent `SR` pour aller plus
> vite

> On peut ne visiter que quelques secteurs (area 5-3 de 0-6 à 1-7 par exemple)
> pour aller plus vite

<details>

```lua
---@type Result
local mission_class_count = {}

local missions = GetAllMission()
for _, v in ipairs(missions) do
    if v.name:find("SR") then
        local class_count = {}

        ---@param arch ScenaricArchetype
        local function ClassCouter(arch)
            local class = arch:GetClassName()
            class_count[class] = (class_count[class] or 0) + 1
        end

        ForEachInZone(v.zone, ClassCouter)

        mission_class_count[v.name] = class_count
    end
end


---@param fn fun(areaX: number, areaY: number, sectorX: number, sectorY: number)
function ForEachSector(fn)
    for areaX = 5, 5 do
        for areaY = 3, 3 do
            for sectorX = 0, 1 do
                for sectorY = 6, 7 do
                    fn(areaX, areaY, sectorX, sectorY)
                end
            end
        end
    end
end

ForEachSector(function(areaX, areaY, sectorX, sectorY)
    for _, loc in pairs { ESectorLocation, EWbSectorLocation }
    do
        local class_count = {}
        local function ClassCounter(arch)
            local class = arch:GetClassName()
            class_count[class] = (class_count[class] or 0) + 1
        end

        ForEachInSector(loc, areaX, areaY, sectorX, sectorY, ClassCounter, {})

        local locStr = ""
        if loc == EWbSectorLocation then locStr = "WB" end
        local key = string.format("%d-%d %d-%d %s", areaX, areaY, sectorX, sectorY, locStr)
        mission_class_count[key] = class_count
    end
end)

WriteCsv(mission_class_count)
```

</details>
