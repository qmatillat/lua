# Séparer en fichier

Pour organiser et partager son code, on peut vouloir le séparer en plusieurs
fichiers.

## `require`

Pour importer un fichier depuis un autre en Lua, la technique officielle
utilise le mot clef `require`. Ce mot clef importe toutes les valeurs globales,
et retourne la valeur retournée par le fichier.

```lua
--- module.lua
local function a() end
local function b() end

return {
    a = a,
    c = 7,
}
```

```lua
local m = require 'module'

m.a();
print(m.c) -- 7
m.b()      -- undefined
```

## `ExecuteScript`

Pour faciliter la gestion des scripts et leur chemin d'inclusion, on peut
utiliser la commande `ExecuteScript` qui supporte l'interpolation `%SCRIPT%`
pour le dossier racine des scripts. Cependant, cette méthode ne supporte pas le
retour de valeur.

```lua
ExecuteScript [[%SCRIPT%/mon/script.lua]]
```

> Attention aux variables globales, une collision peut bloquer un autre script.
>
> Par exemple, si on définit une variable `Dialog`, on perd certaines
> fonctionnalités de Babel.
