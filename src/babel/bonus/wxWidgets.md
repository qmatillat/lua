# wxWidgets

![Multi Choice Dialog](./wxWidgets.png)

```lua
require "wx"

MainWin = wx.wxWindow()
MainWin:SetHWND(GetMainWindowHandle())


local missions = GetAllMission()
local missionNames = {}
for i, mission in ipairs(missions)
do
    missionNames[i] = mission.name
end

local d = wx.wxMultiChoiceDialog(
	MainWin,
	"Select your favorite mission",
	"Select a mission",
	missionNames
)
d:ShowModal()

local ids = d:GetSelections()
for i = 0, ids:GetCount()-1 do
	print("###")
	local mission = missions[ids:Item(i) + 1]
	print(mission.name)
	print(mission.owner)
	print(mission.path)
	print(mission.status)
	print(mission.zone)
	print()
end
```
