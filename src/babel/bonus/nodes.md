# Nodes

```lua
local db = Babel:IScenaricDatabase()
local zoneId = UniqueId:FromStringValue("5908722711110096035")
local depot = db:GetZoneDepot(zoneId)

---@param n ITNode
---@param fn fun(n: ITNode, depth: number)
---@param depth? integer
local function forEachNode(n, fn, depth)
    depth = depth or 0
    print(string.rep("	", depth) .. "- " .. n:GetName(), n)
    fn(n, depth)
    for _, v in ipairs(n:GetChildren()) do
        forEachNode(v, fn, depth + 1)
    end
end

local c = {}
forEachNode(depot, function(n)
    if not n:IsKindOf("ITMeshNode") then
        return
    end

    local glmRef = n:GetProperty("GlmRef")
    local glm = glmRef:GetTargetPtr()
    local file = glm:GetOwnerFile():GetName()
    c[file] = (c[file] or 0) + 1
end)

for k, v in pairs(c) do
    print(k, v)
end
```
