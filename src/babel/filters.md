# Filtrer lors de l'itération

Si on est intéressé que sur certains archétypes, on peut ajouter un système de
filtre.

Plusieurs versions, avec de plus en plus de fonctionnalités :
- Filtrer en fonction de la classe
- Ajouter sur `ForEachChild` l'option d'inclure les archétypes intermédiaires.
- Filtrer en fonction de la valeur d'un champ
- Convertir la structure en classe
