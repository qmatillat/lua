# Afficher les résultats

**Objectifs :** exporter les résultats dans une forme exploitable: un CSV

<details>

```lua
---@alias FxInfo {costPlayer: number, costNonPlayer: number, maxParticle: number, categ: string}
---@alias FxResult table<string, FxInfo>

---@param result FxResult
---@param file? file* Optional file, defaults to C:/Temp/fx.csv
function WriteCsv(result, file)
    file = file or io.open("C:\\Temp\\fx.csv", "w+")
    if not file then
        error("File not found")
    end

    file:write("Fx Name;Categ;Cost player;Cost non player;Max particle\n")
    for fx, info in pairs(result) do
        file:write(fx, ";", info.categ, ";", info.costPlayer, ";", info.costNonPlayer, ";", info.maxParticle, "\n")
    end

    file:close()
end
```

</details>
