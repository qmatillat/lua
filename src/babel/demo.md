# Démo du script

| Mission                                                 | Class                 | Count |
|--------------------------------------------------------:|:---------------------:|:------|
| Y1_RC_SR_061_HST_EVENT_001 (WONDERS OF KOÊ»OLAU POKO)   | CITEntityDummyMatrix  | 2     |
| Y1_RC_SR_061_HST_EVENT_001 (WONDERS OF KOÊ»OLAU POKO)   | CITEntityPath         | 1     |
| Y1_TA_SR_007_LAMBO_RACETHESUN (TRAILBLAZING FOREVER)    | CITEntityLightDimArea | 1     |
| Y1_TA_SR_007_LAMBO_RACETHESUN (TRAILBLAZING FOREVER)    | CITEntityAudioSource  | 2     |
| Y1_RC_SR_024_PORSCHE_PORSCHE_EATS_PORSCHE (911 ARRIVAL) | CITEntityLightDimArea | 1     |
| Y1_RC_SR_024_PORSCHE_PORSCHE_EATS_PORSCHE (911 ARRIVAL) | CITEntityDummyMatrix  | 2     |
| S03_TA_SR_003_GONE_MINUTE (SCENE 6: THE HEIST)          | CITEntityLightDimArea | 3     |
| S03_TA_SR_003_GONE_MINUTE (SCENE 6: THE HEIST)          | CITEntityDummyMatrix  | 2     |


```lua
local result = {
    ["Y1_RC_SR_053_LAMBO_LAMBOARDING (CREATING A LEGEND)"] = {
        CITEntityMissionSpawner = 1,
        CITEntityPolygon = 1,
        CITEntityMoviePlayer = 4,
        CITEntityPath = 1,
        CITEntityVideo = 1,
        CITEntityVolumeTrigger = 2,
        CITEntityDummyMatrix = 2,
        CITEntityMission = 1,
        CITEntityAudioSource = 2,
        CITEntityMaterialReplacer = 2,
        CITEntityStartingGrid = 3,
        CITEntityWorldLayerController = 2,
    },
    ["Y1_RC_SR_050_VINTAGE_ANCESTOR (STOCK AROUND THE CLOCK)"] = {
        CITEntityWorldLayerController = 2,
        CITEntityPolygon = 1,
        CITEntityMoviePlayer = 2,
        CITEntityPath = 1,
        CITEntityVoiceMessageDescription = 5,
        CITEntityVolumeTrigger = 8,
        CITEntityDummyMatrix = 2,
        CITEntityMission = 1,
        CITEntityAudioSource = 2,
        CITEntityLightDimArea = 1,
        CITEntityStartingGrid = 3,
        CITEntityMissionSpawner = 1,
    },
    -- [...]
}
```
