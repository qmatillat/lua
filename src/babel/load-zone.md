# Charger / Décharger une zone

**Objectif :** trouver et charger une zone, soit avec son nom, soit avec son ID. 
Puis itérer sur l'ensemble des archétypes de la zone

<details>

```lua
---@param zone UniqueId | string
---@param fn fun(id: UniqueId)
function WithZone(zone, fn)
    if type(zone) == string then
        ---@cast zone string
        zone = db:GetZoneByName(zone)
    end
    ---@cast zone UniqueId

    db:LoadZone(zone, false, false)
    fn(zone)
    db:UnloadZone(zone, false)
end

---@param zone UniqueId | string
---@param fn fun(arch: ScenaricArchetype)
function ForEachInZone(zone, fn)
    WithZone(zone, function(id)
        local entites = db:GetEntitiesByLocation(EZoneLocation, id)
        for _, entityId in ipairs(entites)
        do
            local entity = db:GetEntityOrArchetype(entityId)
            fn(entity)
        end
    end)
end
```

</details>


<hr />

# Charger / Décharger un secteur

**Objectif :** charger un secteur a partir de son numéro (areaX, areaY, sectorX, sectorY)
Puis itérer sur l'ensemble des archétypes du secteur

<details>

```lua
---@param location ITEnumLocation
---@param areaX number
---@param areaY number
---@param sectorX number
---@param sectorY number
---@param fn fun(arch: ScenaricArchetype)
function ForEachInSector(location, areaX, areaY, sectorX, sectorY, fn)
    db:Resolve(location, areaX, areaY, sectorX, sectorY)

    local entites = db:GetEntitiesByLocation(location, areaX, areaY, sectorX, sectorY)
    for _, entityId in ipairs(entites)
    do
        local entity = db:GetEntityOrArchetype(entityId)
        fn(entity)
    end

    db:Unresolve(location, areaX, areaY, sectorX, sectorY)
end
```

</details>
