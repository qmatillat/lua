# Documentation

Comment trouver quelle fonction appeler ? Que peut-on faire dans Babel ? Il y a
3 sources de documentation pour l'API Lua dans Babel :
- Le code `C++`
- La documentation officielle (fichier RTF + affichage dans Babel)
- Une documentation technique, qui peut être chargée dans votre IDE
