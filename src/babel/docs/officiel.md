# Officiel

Pour chaque fonction, il y a un fichier `rtf` avec le nom de la fonction et les paramètres.

`D:\Projects\ORL\Main\Work1\Applications\Game\Sources\GameModes\Babel\Dllx64\Doc`


### Exemple

Chaque nom de fichier suit une convention : c'est la concaténation entre
- Nom de l'objet sur lequel la méthode existe
- Nom de la méthode

Dans l'exemple si dessous, c'est la méthode `GetName` sur `ScenaricEntity`.

![RTF](./rtf.png)

### Dans Babel

Dans Babel, lorsque le déboguer est en pause, un popup affiche aussi la doc.

![In Babel](./inBabel.png)
