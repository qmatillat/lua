# IDE

Une autre option (non officielle) est un fichier Lua qui contient les fonctions
dans un format utilisable par les IDE. Cela permet à l'IDE de comprendre les
fonctions, le type des variables, et du coup, les fonctions que l'on peut
appeler.

> Toutes les fonctions ne sont pas encore exposées. Ce fichier est maintenu par
> les utilisateurs de Lua dans Babel.

`D:\Projects\ORL\Main\Work1\Applications\Game\Sources\GameModes\Babel\Scripts\VehicleChecker\types.lua`

![IDE](./ide.png)
