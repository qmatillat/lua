# Étapes du script

Quelles sont les étapes du script ?

<details>

- Itération sur un arbre d'archétype
- Itération sur des sous membres
- Récupérer les valeurs
  - Nom du summit
  - Nom de la mission
  - Nom de la playlist
  - VCat utilisée
- Afficher les résultats

</details>

Pour simplifier, commencez par extraire les infos d'un seul archétype, puis à
la fin, rajoutez la boucle pour le faire sur chaque archétype.

**Conseil :** commencer le fichier avec :
```lua
local db = Babel:IScenaricDatabase()
```

