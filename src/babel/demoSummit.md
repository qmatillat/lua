# Démo du script (Summit)

<style>
.table-wrapper {
margin: 5px -150px 5px -150px
}
</style>

|                        Summit Name |         Mission        |             Playlist             |        VCat         |
|-----------------------------------:|:----------------------:|:--------------------------------:|:-------------------:|
| S04_W13_PorscheAMotorfestSelection |   LOW' DON'T DANCE N   | Playlist_PL_013_DONK_VS_LOWRIDER |    StreetRacing     |
| S04_W13_PorscheAMotorfestSelection |      FRENCH DREAMS     |    Playlist_PL_006_DREAM_CARS    |     FormulaOne      |
| S04_W13_PorscheAMotorfestSelection |       DENVER RWD       | Playlist_PL_007_AMERICAN_MUSCLES |    StreetRacing     |
| S04_W13_PorscheAMotorfestSelection |       911 LEGENDS      | Playlist_PL_001_A_PORSCHE_STORY  |    StreetRacing     |
| S04_W13_PorscheAMotorfestSelection |     DREAM BUGATTIS     |    Playlist_PL_006_DREAM_CARS    |     FormulaOne      |
| S04_W13_PorscheAMotorfestSelection | STOCK AROUND THE CLOCK |  Playlist_PL_010_VINTAGE_GARAGE  | StreetRacing_Tier_1 |
|                  S04_W14_LBWKReign |    PUNK PERFORMANCE    |   Playlist_PL_014_LIBERTY_WALK   |       Hypercar      |
|                  S04_W14_LBWKReign |     CUSTOM NATION      |   Playlist_PL_014_LIBERTY_WALK   |    StreetRacing     |
|                  S04_W14_LBWKReign |    PURE PERFORMANCE    |   Playlist_PL_014_LIBERTY_WALK   |    StreetRacing     |
|                  S04_W14_LBWKReign |      ELITE NATION      |   Playlist_PL_014_LIBERTY_WALK   |    StreetRacing     |
|                  S04_W14_LBWKReign |    PEAK PERFORMANCE    |   Playlist_PL_014_LIBERTY_WALK   |    StreetRacing     |
|                  S04_W14_LBWKReign |     GLOBAL NATION      |   Playlist_PL_014_LIBERTY_WALK   |    StreetRacing     |

![Excel result](./demoSummit.png)
