# Démo du script

<style>
.table-wrapper {
margin: 5px -150px 5px -150px
}
</style>

|                                     Fx Name |               Categ              | Cost player | Cost non player | Max particle |
|--------------------------------------------:|:--------------------------------:|:-----------:|:---------------:|:------------:|
|             FX_ORL_ENVIROFX_DUSTMOTE_POLLEN |       ArcheFxWeightCategory      |      10     |        1        |      120     |
|           FX_TC2_DRIFT_CUSTO_SMOKE_010_bike | ArcheFxWeight_VFX_Vanities_Smoke |     1000    |       100       |      400     |
|          FX_CTCT_ORL_DIRTGENERIC_ROLL_Front |     ArcheFxWeight_3C_TIREFXS     |     1000    |       100       |      108     |
|      FX_ORL_PLAYLIST_OS_Effectors_CalmWater |       ArcheFxWeightCategory      |      10     |        1        |      36      |
|   FX_CTCT_ORL_DIRT_SULFUR_ROLL_V2_Rear_Left |     ArcheFxWeight_3C_TIREFXS     |     1000    |       100       |      180     |
|           FX_TC2_CUSTO_SMOKE_021_BURN_RIGHT | ArcheFxWeight_VFX_Vanities_Smoke |     1000    |       100       |      100     |
| FX_TC2_SMOKES_TICKET_VANITY01_SET1_TIER3_03 |       ArcheFxWeightCategory      |      10     |        1        |       1      |

![Graph FX](./fxGraph.png)
