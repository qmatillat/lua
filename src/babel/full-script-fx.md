# Script complet

**Objectifs :** Pour chaque FX, rapporter son nom, sa catégorie, son poid
joueur / non-joueur et son nombre max de particules.

```lua
local db = Babel:IScenaricDatabase()

---@param source UniqueId[]
---@param fn fun(arch: ScenaricArchetype)
function ForEachChild(source, fn)
    for _, rootId in ipairs(source)
    do
        local root = db:GetEntityOrArchetype(rootId)
        local child = root:GetInheritedArchetypes()
        if #child == 0 then
            fn(root)
        end

        ForEachChild(child, fn)
    end
end

---@alias FxInfo {costPlayer: number, costNonPlayer: number, maxParticle: number, categ: string}
---@alias FxResult table<string, FxInfo>

---@param result FxResult
---@param file? file* Optional file, defaults to C:/Temp/fx.csv
function WriteCsv(result, file)
    file = file or io.open("C:\\Temp\\fx.csv", "w+")
    if not file then
        error("File not found")
    end

    file:write("Fx Name;Categ;Cost player;Cost non player;Max particle\n")
    for fx, info in pairs(result) do
        file:write(fx, ";", info.categ, ";", info.costPlayer, ";", info.costNonPlayer, ";", info.maxParticle, "\n")
    end

    file:close()
end

---@type FxResult
local res = {}

ForEachChild({ UniqueId:FromStringValue("118887") }, function(arch)
    local costPlayer = 0
    local costNonPlayer = 0
    local maxParticle = 0
    local categName = "UNKNOWN"

    local categId = UniqueId:FromStringValue(arch:GetMemberValue("Priority.FxWeightCategory"))
    if categId:IsValid() then
        local categ = db:GetEntityOrArchetype(categId)
        categName = categ:GetName()
        costPlayer = tonumber(categ:GetMemberValue("WeightIfPlayer")) or 0
        costNonPlayer = tonumber(categ:GetMemberValue("WeightIfNotPlayer")) or 0
    end

    local systemId = UniqueId:FromStringValue(arch:GetMemberValue("ParticleFXArchetype"))
    if systemId:IsValid() then
        local system = db:GetEntityOrArchetype(systemId)
        local n = system:GetSubMembersCount("EmittersParams")
        for i = 0, (n - 1) do
            local emitterIdStr = system:GetMemberValue("EmittersParams.[" .. i .. "].EmitterFXID")
            local emitterId = UniqueId:FromStringValue(emitterIdStr)
            if emitterId:IsValid() then
                local emitter = db:GetEntityOrArchetype(emitterId)
                local particle = tonumber(emitter:GetMemberValue(
                    "EmissionModifier.EmitterGroupEmission.EmitterGroupParticlesCount.EmitterMaxParticleCount"))
                maxParticle = math.max(maxParticle, particle or 0)
            end
        end
    end

    res[arch:GetName()] = {
        costPlayer = costPlayer,
        costNonPlayer = costNonPlayer,
        maxParticle = maxParticle,
        categ = categName,
    }
end)

WriteCsv(res)
```
