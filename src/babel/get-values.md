# Récuperation des valeurs

**Objectifs :** extraire de Babel nos informations

> <center>
>
> Pour récupérer le chemin d'où l'info est enregistrée, <br />
> on peut se faire aider par Babel :
> 
> ![Get member path in Babel](./getMemberPath.png)
>
> </center>

On peut récupérer (par ordre de difficulté)
- Nom de la catégorie
- *Weight* si (non) joueur
- Les max particles counts

<details>

### Nom de catégorie
```lua
local categId = UniqueId:FromStringValue(arch:GetMemberValue("Priority.FxWeightCategory"))
if categId:IsValid() then
    local categ = db:GetEntityOrArchetype(categId)
    categName = categ:GetName()
end
```

### *Weight* si (non) joueur
```lua
local categId = UniqueId:FromStringValue(arch:GetMemberValue("Priority.FxWeightCategory"))
if categId:IsValid() then
    local categ = db:GetEntityOrArchetype(categId)
    costPlayer = tonumber(categ:GetMemberValue("WeightIfPlayer")) or 0
    costNonPlayer = tonumber(categ:GetMemberValue("WeightIfNotPlayer")) or 0
end
```

### Les max particles counts
Ici, le max du MaxParticleCount de chaque émetteur
```lua
local maxParticle = 0
local systemId = UniqueId:FromStringValue(arch:GetMemberValue("ParticleFXArchetype"))
if systemId:IsValid() then
    local system = db:GetEntityOrArchetype(systemId)
    local n = system:GetSubMembersCount("EmittersParams")
    for i = 0, (n - 1) do
        local emitterIdStr = system:GetMemberValue("EmittersParams.[" .. i .. "].EmitterFXID")
        local emitterId = UniqueId:FromStringValue(emitterIdStr)
        if emitterId:IsValid() then
            local emitter = db:GetEntityOrArchetype(emitterId)
            local particle = tonumber(emitter:GetMemberValue(
                "EmissionModifier.EmitterGroupEmission.EmitterGroupParticlesCount.EmitterMaxParticleCount"))
            maxParticle = math.max(maxParticle, particle or 0)
        end
    end
end
```

### Exemple complet
```lua
local costPlayer = 0
local costNonPlayer = 0
local maxParticle = 0
local categName = "UNKNOWN"

local categId = UniqueId:FromStringValue(arch:GetMemberValue("Priority.FxWeightCategory"))
if categId:IsValid() then
    local categ = db:GetEntityOrArchetype(categId)
    categName = categ:GetName()
    costPlayer = tonumber(categ:GetMemberValue("WeightIfPlayer")) or 0
    costNonPlayer = tonumber(categ:GetMemberValue("WeightIfNotPlayer")) or 0
end

local systemId = UniqueId:FromStringValue(arch:GetMemberValue("ParticleFXArchetype"))
if systemId:IsValid() then
    local system = db:GetEntityOrArchetype(systemId)
    local n = system:GetSubMembersCount("EmittersParams")
    for i = 0, (n - 1) do
        local emitterIdStr = system:GetMemberValue("EmittersParams.[" .. i .. "].EmitterFXID")
        local emitterId = UniqueId:FromStringValue(emitterIdStr)
        if emitterId:IsValid() then
            local emitter = db:GetEntityOrArchetype(emitterId)
            local particle = tonumber(emitter:GetMemberValue(
                "EmissionModifier.EmitterGroupEmission.EmitterGroupParticlesCount.EmitterMaxParticleCount"))
            maxParticle = math.max(maxParticle, particle or 0)
        end
    end
end

print(arch:GetName(), categName, costPlayer, costNonPlayer, maxParticle)
```

</details>
