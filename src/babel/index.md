# Babel

<style>
blockquote {
    background-color: rgb(176, 28, 28);
    border-radius: 4px;
}
</style>

> <h1>&#x26A0; Attention !</h1>
>
> Les Lua est un outil puissant qui permet de manipuler Babel à une couche
> inférieur que via l'interface graphique.
>
> Il **FAUT** donc faire attention aux manipulations que l'on fait via le Lua, il
> est possible de faire des choses que serait impossible via l'interface
> graphique.
>
> Quand on modifie des données, il est **OBLIGATOIRE** de faire un *Recheck
> All* à la fin pour vérifier que tout est correct. Attention aux logs, les
> warnings et autres messages peuvent être important.

### Exemple de code dangereux

```lua
local db = Babel:IScenaricDatabase()

-- id2 is a child of id1
local id1 = UniqueId:FromStringValue("144115188076328201")
local id2 = UniqueId:FromStringValue("144115188076328308")

local arch1 = db:GetEntityOrArchetype(id1)

arch1:Reparent(id2)
arch1:SetMemberValue("ReplacementWhenOwned.ForCars", "This is not an ID")
```
