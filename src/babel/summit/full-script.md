# Script complet

```lua
local CSV = require "G2W.CSV"
local db = Babel:IScenaricDatabase()

---@param source UniqueId[]
---@param fn fun(arch: ScenaricArchetype)
local function ForEachChild(source, fn)
    for _, rootId in ipairs(source)
    do
        local root = db:GetEntityOrArchetype(rootId)
        local child = root:GetInheritedArchetypes()
        if #child == 0 then
            fn(root)
        end

        ForEachChild(child, fn)
    end
end

---@param id UniqueId
---@return MissionEntry?
local function FindMissionFromId(id)
    for _, mission in ipairs(GetAllMission()) do
        if mission.spawnerId == id then
            return mission
        end
    end
end

local csv = CSV:new { "Summit", "Mission", "Playlist", "Restriction", "VCat" }

---@param arch ScenaricArchetype
---@param prefix string
local function HandleActivity(arch, prefix)
    local missionId = UniqueId:FromStringValue(arch:GetMemberValue(prefix .. "MissionId"))
    if not missionId:IsValid() then
        return -- Most likely a skill
    end

    local restrictionId = UniqueId:FromStringValue(
        arch:GetMemberValue(prefix .. "EventConstraints.VehicleRestrictionID")
    )

    local vcat = "<UNCHANGED>"
    local restrictionName = "<NONE>"
    if restrictionId:IsValid() then
        restrictionName = db:GetEntityOrArchetype(restrictionId)
            :GetName()
            :gsub("VehicleRestriction_Summit_", "")

        local veh = UniqueId:FromStringValue(
            db:GetEntityOrArchetype(restrictionId):GetMemberValue("DefaultVehicleConfig")
        )
        local dress = UniqueId:FromStringValue(
            db:GetEntityOrArchetype(veh):GetMemberValue("CarDress")
        )

        vcat = db:GetEntityOrArchetype(dress):GetName():gsub("CarDress_", "")
    end

    local mission = FindMissionFromId(missionId)
    local name
    local playlist

    if mission then
        name = mission.name:gsub(".*%(", ""):gsub("%)", "")

        local pl = db:GetEntityOrArchetype(mission.playlistId)
        if pl then
            playlist = pl:GetName()
        end
    end

    csv:add_row {
        Summit = arch:GetName(),
        Mission = name,
        Playlist = playlist,
        Restriction = restrictionName,
        VCat = vcat,
    }
end

ForEachChild({ UniqueId:FromStringValue("576460752303689311") }, function(arch)
    local activities = arch:GetSubMembersCount("SummitActivities")
    for i = 0, activities - 1 do
        HandleActivity(arch, "SummitActivities.[" .. i .. "].")
    end
end)

csv:write_to(io.open("D:/summit.csv", "w+"))
```
