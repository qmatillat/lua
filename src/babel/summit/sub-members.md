# Itération sur des sous-membres

**Objectif** : afficher les infos récupérées précédemment, mais pour toutes les
missions d'un summit.

Les chemins des membres de la liste ont un point commun, est-ce qu'on peut
l'utiliser pour accéder à chaque élément programmatiquement ?

<details>

```lua
---@param arch ScenaricArchetype
---@param prefix string
local function HandleActivity(arch, prefix)
    local missionId = UniqueId:FromStringValue(arch:GetMemberValue(prefix .. "MissionId"))
    local restrictionId = UniqueId:FromStringValue(
        arch:GetMemberValue(prefix .. "EventConstraints.VehicleRestrictionID")
    )
    -- [...]
end

local activities = arch:GetSubMembersCount("SummitActivities")
for i = 0, activities - 1 do
    HandleActivity(arch, "SummitActivities.[" .. i .. "].")
end
```

</details>
