# Afficher les résultats

**Objectif** : écrire les infos dans un CSV.


### Exemple d’utilisation

```lua
local CSV = require "G2W.csv"
local csv = CSV:new { "A", "B", "C" }
csv:add_row { A = 7, B = 0, C = "Test" }
print(csv:to_string())
-- csv:write_to(file)
```


<details>

```lua
local CSV = require "G2W.CSV"

local csv = CSV:new { "Summit", "Mission", "Playlist", "Restriction", "VCat" }

---@param arch ScenaricArchetype
---@param prefix string
local function HandleActivity(arch, prefix)
    --- [...]

    csv:add_row {
        Summit = arch:GetName(),
        Mission = name,
        Playlist = playlist,
        Restriction = restrictionName,
        VCat = vcat,
    }
end

ForEachChild({ UniqueId:FromStringValue("576460752303689311") }, function(arch)
    local activities = arch:GetSubMembersCount("SummitActivities")
    for i = 0, activities - 1 do
        HandleActivity(arch, "SummitActivities.[" .. i .. "].")
    end
end)

csv:write_to(io.open("D:/summit.csv", "w+"))
```

</details>
