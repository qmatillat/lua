# Playlist

**Objectif** : récupérer le nom de la playlist

<details>

```lua
---@param id UniqueId
---@return MissionEntry?
local function FindMissionFromId(id)
    for _, mission in ipairs(GetAllMission()) do
        if mission.spawnerId == id then
            return mission
        end
    end
end

local missionId = UniqueId:FromStringValue(arch:GetMemberValue(
    "SummitActivities.[0].MissionId"
))
if not missionId:IsValid() then
    return
end

local mission = FindMissionFromId(missionId)
if not mission then
    return
end

local pl = db:GetEntityOrArchetype(mission.playlistId)
if not pl then
    return
end
print(pl:GetName())
```

</details>
