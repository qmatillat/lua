# Mission

**Objectif** : récupérer le nom de la mission

<details>

```lua
---@param id UniqueId
---@return MissionEntry?
local function FindMissionFromId(id)
    for _, mission in ipairs(GetAllMission()) do
        if mission.spawnerId == id then
            return mission
        end
    end
end

local missionId = UniqueId:FromStringValue(arch:GetMemberValue(
    "SummitActivities.[0].MissionId"
))
if not missionId:IsValid() then
    return
end

local mission = FindMissionFromId(missionId)
if not mission then
    return
end

print(mission.name:gsub(".*%(", ""):gsub("%)", ""))
```

</details>
