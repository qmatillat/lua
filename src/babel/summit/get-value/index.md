# Récupération des valeurs

**Objectifs :** extraire de Babel nos informations

<style>
blockquote { text-align: center; }
</style>

> Pour récupérer le chemin d'où l'info est enregistrée, on peut se faire aider
> par Babel :
> 
> ![Copy member path in Babel](../../getMemberPath.png)

On veut récupérer (par ordre de difficulté)
1. Nom du summit
1. Nom de la mission utilisée
1. Nom de la playlist de la mission
1. VCat utilisée dans cette mission

> Pour commencer, on peut utiliser l'archétype suivant : `144115188076386959`,
> et récupérer uniquement les infos de la première mission.

