# VCat utilisé

**Objectif** : récupérer la VCat utilisée sur la mission

Il y a plusieurs chemins possibles, ici, j'ai utilisé la VCat du véhicule prêté
(le véhicule par défaut de la restriction).

<details>

```lua
local restrictionId = UniqueId:FromStringValue(
    arch:GetMemberValue("SummitActivities.[0].EventConstraints.VehicleRestrictionID")
)

local vcat = "<UNCHANGED>"
local restrictionName = "<NONE>"
if restrictionId:IsValid() then
    restrictionName = db:GetEntityOrArchetype(restrictionId)
        :GetName()
        :gsub("VehicleRestriction_Summit_", "")

    local veh = UniqueId:FromStringValue(
        db:GetEntityOrArchetype(restrictionId):GetMemberValue("DefaultVehicleConfig")
    )
    local dress = UniqueId:FromStringValue(
        db:GetEntityOrArchetype(veh):GetMemberValue("CarDress")
    )

    vcat = db:GetEntityOrArchetype(dress):GetName():gsub("CarDress_", "")
end
print(vcat, restrictionName)
```

</details>
