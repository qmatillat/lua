# Summit

**Objectif** : récupérer le nom du summit

<details>

```lua
local db = Babel:IScenaricDatabase()

local rootId = UniqueId:FromStringValue("144115188076386959")
local arch = db:GetEntityOrArchetype(rootId)

print(arch:GetName())
```

</details>
