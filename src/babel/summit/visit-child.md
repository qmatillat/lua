# Itération sur un arbre d'archétype

**Objectif** : visiter tous les archétypes finaux

Cette fonction doit être réutilisable dans différents contextes

<details>

```lua
---@param source UniqueId[]
---@param fn fun(arch: ScenaricArchetype)
local function ForEachChild(source, fn)
    for _, rootId in ipairs(source)
    do
        local root = db:GetEntityOrArchetype(rootId)
        local child = root:GetInheritedArchetypes()
        if #child == 0 then
            fn(root)
        end

        ForEachChild(child, fn)
    end
end
```

</details>
