# Se faire aider par l'IDE

Pour se simplifier la vie, on peut annoter la plupart des instructions. Cela
permet à l'IDE de comprendre ce que l'on veut faire, et il va nous aider à
respecter certaines règles, notamment de typage.

Si on prend le code ci-dessous, quelles sont les erreurs :

```lua
function Max(a, b)
    if a > b then
        return a
    else
        return b
    end
end

function MaxArr(arr)
    if #arr == 0 then
        return
    end

    local max = arr[1]
    for _, v in ipairs(arr)
    do
        max = Max(max, v)
    end
    return max
end

local a = "7"
local b = "12"

print(Max(a, b))
print(MaxArr { a, b, "15" })
print(Max(MaxArr { a, b }, 0))
```

<details>

```lua
---@param a number
---@param b number
---@return number
function Max(a, b)
    if a > b then
        return a
    else
        return b
    end
end

---@param arr number[]
---@return number?
function MaxArr(arr)
    if #arr == 0 then
        return
    end

    local max = arr[1]
    for _, v in ipairs(arr)
    do
        max = Max(max, v)
    end
    return max
end

local a = 7
local b = 12

print(Max(a, b))
print(MaxArr { a, b, 15 })
print(Max(MaxArr { a, b } or 0, 0))
```

</details>
