# Initiation a LUA

Il s'agit d'un cours sur le LUA d'environ 6 heures, sur 2 demi-journées. Le
cours couvre les débuts avec LUA, de la syntaxe de base aux scripts plus
complets.

Le but de ce cours et de vous apprendre à vous servir de LUA. Ce cours
s'adresse aux débutants et part de zéro. Les principaux objectifs sont :

* Vous donnez une compréhension complète de la syntaxe et du langage.
* Vous permettre de modifier des programmes existants et d'en écrire de nouveaux
  en LUA
* Vous montrez les concepts courant en LUA.

Dans la seconde séance, nous allons utiliser le LUA pour scripter dans Reaper.

## Non-Objectifs

Bien que le LUA soit un langage simple, il n'en ait pas moins flexible. Dans ce
cours non n'allons par aborder les concepts les plus avancées, comme :
* Les coroutines.
* Intégration dans une application C.

## Prérequis

Ce cours n'a pas de prérequis, nous allons partir de la base
