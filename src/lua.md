# Utiliser du LUA

## Installation

Pour simplifier le développement, l'utilisation d'un IDE (Environnent de
Développent Intégré) est conseillé. Nous conseillons [VS Code][vscode] avec
l'extension [LUA][lua] (`sumneko.lua`).

[vscode]: https://code.visualstudio.com/
[lua]: https://marketplace.visualstudio.com/items?itemName=sumneko.lua
